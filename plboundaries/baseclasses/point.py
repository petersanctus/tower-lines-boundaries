from functools import total_ordering

class LasPoint_3():
    """
    Format 3:
    "X": "i4",
    "Y": "i4",
    "Z": "i4",
    "intensity": "u2",
    "bit_fields": "u1",
    "raw_classification": "u1",
    "scan_angle_rank": "i1",
    "user_data": "u1",
    "point_source_id": "u2",
    "gps_time": "f8",
    "red": "u2",
    "green": "u2",
    "blue": "u2"   
    """

    # sub fields of the 'bit_fields' dimension
    RETURN_NUMBER_MASK_0 = 0b00000111
    NUMBER_OF_RETURNS_MASK_0 = 0b00111000
    SCAN_DIRECTION_FLAG_MASK_0 = 0b01000000
    EDGE_OF_FLIGHT_LINE_MASK_0 = 0b10000000

    # sub fields of the 'raw_classification' dimension
    CLASSIFICATION_MASK_0 = 0b00011111
    SYNTHETIC_MASK_0 = 0b00100000
    KEY_POINT_MASK_0 = 0b01000000
    WITHHELD_MASK_0 = 0b10000000   

    def __init__(self, np_line, scale): 
        self._x                  = np_line['X']
        self._y                  = np_line['Y']
        self._z                  = np_line['Z']
        self._scale              = scale
        self._gpstime            = np_line['gps_time']
        self._raw_classification = np_line['raw_classification']
        self._intensity          = np_line['intensity']
        self._bit_fields         = np_line['bit_fields']
        
        
    @property
    def x(self) -> int:
        return self._x*self.scale
    
    @property
    def y(self) -> int:
        return self._y*self.scale

    @property
    def z(self) -> int:
        return self._z*self.scale

    @property
    def intensity(self) -> int:
        return self._intensity

    @property
    def gpstime(self) -> int:
        return self._gpstime

    @property
    def bit_fields(self):
        return self._bit_fields

    @property
    def return_number(self) -> int:
        return LasPoint_3.RETURN_NUMBER_MASK_0 & self._bit_fields

    @property
    def number_of_returns(self) -> int:
        return (LasPoint_3.NUMBER_OF_RETURNS_MASK_0 & self._bit_fields) >> 3

    @property
    def classification(self) -> int:
        return LasPoint_3.CLASSIFICATION_MASK_0 & self._raw_classification

    @property
    def raw_classification(self) -> int:
        return self._raw_classification

    @property
    def scale(self) -> int:
        return self._scale

    @property
    def id(self) -> int:
        return self._id

    def higher_then(self, other) -> bool:
        return self.z > other.z
    
    def __gt__(self, other) -> bool:
        return self.z > other.z

    def point_to_array(self):
        return [self.x, self.y, self.z]

    def __str__(self) -> str:
        return f'{self.x} {self.y} {self.z} {self.classification} {self.gpstime} ' + \
               f'{self.intensity} {self.return_number} {self.number_of_returns}'

@total_ordering
class Point2D():


    def __init__(self, x, y, id='0', scale=1):
        self._id = id
        self._scale = scale
        self._x = x
        self._y = y

    @property
    def x(self):
        return self._x * self.scale    
    
    @property
    def y(self):
        return self._y * self.scale    

    @property
    def scale(self):
        return self._scale
        
    @property
    def id(self) -> int:
        return self._id

    @id.setter
    def id(self, value: str):
        self._id = value

    def __eq__(self, other) -> bool:
        return self.id == other.id

    def __str__(self) -> str:
        return f'{self.id} {self.x:.2f} {self.y:.2f}'

    def __gt__(self, other) -> bool:
        if len(self.id.split('.')) == 1 or len(other.id.split('.')) == 1:
            return str(self.id) > str(other.id)
        elif len(self.id.split('.')) == 2 and len(other.id.split('.')) == 2:
            twr1 = int(self.id.split('.')[1])
            twr2 = int(other.id.split('.')[1])
            return twr1 > twr2

    def __ne__(self, other):
        return not (self.id == other.id)

    def __lt__(self, other):
        if len(self.id.split('.')) == 1 or len(other.id.split('.')) == 1:
            return str(self.id) <= str(other.id)
        elif len(self.id.split('.')) == 2 and len(other.id.split('.')) == 2:
            twr1 = int(self.id.split('.')[1])
            twr2 = int(other.id.split('.')[1])
            return twr1 <= twr2

    def __repr__(self):
        return "%s" % (self.id)

class Point3D(Point2D):
    def __init__(self, x, y, z, id='0'):
        self._z = z
        super().__init__(x, y)

    @property
    def z(self):
        return self._z * self.scale

    def higher_then(self, other) -> bool:
        return self.z > other.z
    