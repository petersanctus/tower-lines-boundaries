from ezdxf.entities import line
from plboundaries.baseclasses.point import *
import math
import numpy as np
from shapely.geometry import Point
from shapely.geometry import Polygon
from shapely.geometry import LineString

class Span():

    def __init__(self, 
                 twr1: Point2D, 
                 twr2: Point2D, 
                 buff_transv: float =10, 
                 long_buff: float =0):
        self._tower_1 = twr1
        self._tower_2 = twr2
        self._buff_transv = buff_transv
        self._long_buff = long_buff
        self.calculate_boundary_pts()
        

    @property
    def id(self) -> str:
        ids = [str(self._tower_1.id), str(self._tower_2.id)]
        ids.sort()
        return f"{ids[0]}-{ids[1]}"
        
    @property
    def tower_1(self) -> Point2D:
        return self._tower_1

    @property
    def tower_2(self) -> Point2D:
        return self._tower_2

    @property
    def t1(self) -> np.ndarray:
        return np.array([self.tower_1.x, self.tower_1.y])

    @property
    def t2(self) -> np.ndarray:
        return np.array([self.tower_2.x, self.tower_2.y])

    @property
    def centroid(self):        
        return self._centroid

    @property
    def v12(self) -> np.ndarray:
        return self.t2 - self.t1 # type: ignore

    @property
    def j(self):
        return self._j

    @property
    def v1j(self):
        return self.j - self.t1

    @property
    def v1k(self):
        return [-self.v1j[1], self.v1j[0]]

    @property
    def a(self):                
        return self._a
        
    @property
    def b(self):   
        return self._b

    @property
    def c(self):
        return self._c
        
    @property
    def d(self):    
        return self._d     

    @property
    def e(self):
        return self._e
        
    @property
    def f(self):
        return self._f    
     
    @property
    def buff_transv(self) -> float:
        return self._buff_transv

    @property
    def long_buff(self) -> float:
        return self._long_buff

    @property
    def boundary(self) -> Polygon:
        return self._boundary


    @tower_1.setter
    def tower_1(self, value: Point2D):
        self._tower_1 = value        

    @tower_2.setter
    def tower_2(self, value: Point2D):
        self._tower_2 = value

    def contains(self, point: Point) -> bool:
        return self.boundary.contains(point)
            
        
    '''
    :          A-----k------------P----+-------------------l-----B
    :                |   .                                 |
    :                |      .                              |
    :          F----T1-------j----w------------------------T2----C
    :                |                                     |
    :                |                                     |
    :          E-----n------------------+------------------m-----D
    '''
    def calculate_boundary_pts(self):
        dist_t1_t2 = math.sqrt(np.dot(self.v12, self.v12))
        if dist_t1_t2 == 0:
            raise ValueError(f'Twr\'s distance is 0. Twr1: {self.tower_1.id}; Twr2: {self.tower_2.id}')
        """ t1_t2 = LineString([(self.tower_1.x, self.tower_1.y), 
                            (self.tower_2.x, self.tower_2.y)])
        t1_t2_l = t1_t2.parallel_offset(self.buff_transv, side='left')
        t1_t2_r = t1_t2.parallel_offset(self.buff_transv, side='right')
        self._a, self._e = t1_t2_l.coords[0] """
        self._f = self.t1 - self.long_buff / dist_t1_t2 * self.v12
        self._j = self.t1 + self.buff_transv / dist_t1_t2 * self.v12
        self._a = ([round(self.v1k[0] + self.f[0], 4), round(self.v1k[1] + self.f[1], 4)])
        self._c = self.t2 + self.long_buff / dist_t1_t2 * self.v12
        self._b = ([round(self.v1k[0] + self.c[0], 4), round(self.v1k[1] + self.c[1], 4)])
        self._d = ([round(-self.v1k[0] + self.c[0], 4), round(-self.v1k[1] + self.c[1], 4)])
        self._e = ([round(-self.v1k[0] + self.f[0], 4), round(-self.v1k[1] + self.f[1], 4)])
        self._boundary = Polygon((self._a, self._b, self._d, self.e, self._a))
        centroid = self.t1 + 0.5 * (self.t2-self.t1) # type: ignore
        self._centroid = Point2D(centroid[0], centroid[1], self.id)

if __name__ == "__main__":
    a = np.array([1,1])
    b = np.array([1,2])
    c = a+b
    d = 1
    a = np.ones((2,2)) - np.ones((2,2))
        