import math
from plboundaries.baseclasses.point import *
import numpy as np

class Circle():

    def __init__(self, point, radius):
        self._point_origin = point        
        self._radius = radius

    @property
    def x(self):
        return self._point_origin.x
    
    @property
    def y(self):
        return self._point_origin.y

    @property
    def z(self):
        return self._point_origin.z

    @property
    def radius(self):
        return self._radius
        
    @property
    def point_origin(self):        
        return self._point_origin

    @property
    def numpy_2D(self):
        return np.array([self.x, self.y])

    def contains_point(self, point) -> bool:
        x_dif = point.x - self.x
        y_dif = point.y - self.y
        return round(math.hypot(x_dif, y_dif), 4) <= self._radius


    def contains_boundary(self, qt_boundary) -> bool:
        for point in qt_boundary.boundary_polygon:
            if not self.contains_point(Point2D(point[0], point[1])):
                return False
        return True


    def intersects_boundary(self, qt_boundary) -> bool:
        boundary_polygon = qt_boundary.boundary_polygon
        for p1, p2 in zip(boundary_polygon[::1], boundary_polygon[1::1]):
            p1 = Point2D(p1[0], p1[1])
            p2 = Point2D(p2[0], p2[1])
            if qt_boundary.contains_point(Point2D(self.x, self.y)):
                return True            
            if self.contains_point(p1) or self.contains_point(p2):
                return True
            is_line_inside_circle = self.line_cross_circle(p1, p2)
            w = is_line_inside_circle[1]
            if is_line_inside_circle:
                if w >= 0 and w <= 1:
                    return True
        return False    

    
    def line_cross_circle(self, p1, p2) -> bool:
        c   = np.array([self.x, self.y])
        p1  = np.array([p1.x, p1.y])
        p2  = np.array([p2.x, p2.y])
        v1c = c - p1
        v12 = p2 - p1
        w   = np.dot(v1c, v12) / np.dot(v12, v12)
        pw  = p1 + w * v12
        vwp = c - pw
        sqdist_transv = np.dot(vwp, vwp)
        if sqdist_transv <= self.radius * self.radius:
            return (True, w)
        return (False, w)
