import laspy
import numpy as np
from .point import *
from scipy.spatial import cKDTree

class Cables:

    def __init__(self, las_file: laspy.lasdata.LasData):
        self._cable_points = las_file.points.array
        self._cable_pt_qt = self._2dtree_of(self.cable_points, las_file.header.scales[0])
        self._scale = las_file.header.scales[0]


    @property
    def cable_points(self) -> np.ndarray:
        return self._cable_points

    @property
    def qt_points(self) -> cKDTree:
        return self._cable_pt_qt    

    def _2dtree_of(self, data: np.ndarray, scale: int):
        m  = list(data['X']*scale)
        p  = list(data['Y']*scale)
        data_xy = np.array((m,p)).transpose()
        return cKDTree(data_xy)

    @property
    def scale(self) -> float:
        return self._scale
        