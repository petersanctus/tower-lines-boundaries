from plboundaries.baseclasses.point import *

class Rectangle():

    """
    A rectangle defined via an anchor point *xy* and its *width* and *height*.

    The rectangle extends from Ox + width in positive and negative x-direction
    and from Oy + height in positive and negative y-direction.

      :                +------------------+------------------+
      :                |                  |                  |
      :                |                height               |
      :                |                  |                  |
      :                |                 (xy)---- width -----+
      :                |                                     |
      :                |                                     |
      :                |                                     |
      :                +------------------+------------------+
    
    """   
    
    def __init__(self, origin: Point2D, w, h):        
        self._origin = origin
        self._width = w
        self._height = h

    @property
    def origin(self):
        return self._origin

    @property
    def x(self):
        return self.origin.x
    
    
    @property               
    def y(self):
        return self.origin.y
        
        
    @property
    def width(self):
        return self._width
    
    
    @property               
    def height(self):
        return self._height

    @property
    def boundary(self) -> list:
        return [  Point2D(self.x - self.width, self.y + self.height) ,
                  Point2D(self.x + self.width, self.y + self.height) ,
                  Point2D(self.x + self.width, self.y - self.height) ,
                  Point2D(self.x - self.width, self.y - self.height) ,
                  Point2D(self.x - self.width, self.y + self.height) ]


    @property
    def boundary_polygon(self) -> list:
        return [  [self.x - self.width, self.y + self.height] ,
                  [self.x + self.width, self.y + self.height] ,
                  [self.x + self.width, self.y - self.height] ,
                  [self.x - self.width, self.y - self.height] ,
                  [self.x - self.width, self.y + self.height] ]

    
    def contains_point(self, point) -> bool:        
        return point.x <= self.x + self.width  \
          and point.x >= self.x - self.width   \
          and point.y <= self.y + self.height  \
          and point.y >= self.y - self.height

    
    def contains_boundary(self, rectangle) -> bool: 
        return rectangle.x - rectangle.width >= self.x - self.width and  \
               rectangle.x + rectangle.width <= self.x + self.width and  \
               rectangle.y - rectangle.width >= self.y - self.height and \
               rectangle.y + rectangle.width <= self.x + self.height


    def intersects_boundary(self, rectangle) -> bool:
        return not (rectangle.x - rectangle.width > self.x + self.width or   \
                    rectangle.x + rectangle.width < self.x - self.width or   \
                    rectangle.y - rectangle.height > self.y + self.height or \
                    rectangle.y + rectangle.height < self.y - self.height)   
    

    def __str__(self):
        return "x: {0} ".format(self.x)               \
             + "y: {0} ".format(self.y)               \
             + "h: {0} ".format(self.height)          \
             + "w: {0} ".format(self.width)           \
             + "\nPolygon: \n"                        \
             + "["  + str(self.x - self.width) + ", " \
             + str(self.y + self.height)              \
             + "\n" + str(self.x + self.width) + ", " \
             + str(self.y + self.height)              \
             + "\n" + str(self.x + self.width) + ", " \
             + str(self.y - self.height)              \
             + "\n" + str(self.x - self.width) + ", " \
             + str(self.y - self.height) + "]"