from plboundaries.project import Project
import time
import logging

def crop_macro(input_line: Project, proc_options):
        start = time.perf_counter()
        logging.info(f'++++ Crop Macro initiated on project: {input_line.prj_name}:')
        input_line.ext_twr_names = f'{proc_options["work_folder"]}\\{proc_options["twr_names_file"]}'
        input_line.import_twrs_and_cables()
        input_line.identify_towers(search_buffer = proc_options["search_buffer_cluster_twrs"])
        input_line.find_connections(proc_options)
        input_line.rename_twrs(prefix=proc_options['prefix'])
        input_line.output_twrs_to_txt()
        input_line.output_to_dxf()
        input_line.output_twrs_and_cables_las()
        if proc_options['crop_files']:
            input_line.crop_las_to_span_dirs(sampling=proc_options['sample_step'], 
                                            skip=proc_options['skip files to crop'])
        input_line.merge_twr_names_files()
        finish = time.perf_counter()
        logging.info(f'----------------------Crop Macro finished in {round(finish-start,2)} second(s)')
        return input_line.twr_ident.count


def correct_rn_macro(line_test: Project):
    start = time.perf_counter()
    logging.info(f'++++ Correct Return Numbers Macro initiated on project: {line_test.prj_name}:')
    line_test.correct_rn(line_test.data_folder, 
                        f'{line_test.data_folder}\\RN_Fixed')
    finish = time.perf_counter()
    logging.info(f'----------------------Correct Return Numbers Macro finished in {round(finish-start,2)} second(s)')