from laspy.lasdata import LasData
from laspy.point.record import PackedPointRecord
from plboundaries.tools import delivers
from tqdm import tqdm
from pathlib import Path
import laspy
import numpy as np
import logging
import copy
import os 
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from plboundaries.project import Project

def twrs_and_cables(files_to_crop: list, twrs_sample=1, cables_sample=1) -> tuple:
    twr_pts       = np.empty((0,), dtype='<f8')
    cable_pts     = np.empty((0,), dtype='<f8')
    try:
        logging.info(f'Reading las files')
        for it, file in enumerate(tqdm(files_to_crop, 
                                        desc='Import LiDAR data', 
                                        total=len(files_to_crop))):
            las_file  = laspy.read(file)
            twr_pts   = _get_pts(twr_pts, las_file, 15, twrs_sample)
            cable_pts = _get_pts(cable_pts, las_file, 16, cables_sample)            
        logging.info(f'Imported {it} las files.')
        return _new_las_data(twr_pts, las_file.header), \
               _new_las_data(cable_pts, las_file.header)
    except Exception as e:
        logging.exception('')


def import_external_connections(work_folder, filename: str, prj_name: str) -> list:
    conns = []
    try:        
        if filename == '':
            return conns
        with open(f'{work_folder}\\{filename}', 'r') as f_in:
            file_lines = f_in.read().splitlines()
        first, last = _get_lines_of_interest(file_lines, prj_name)
        if first != -1 and last != -1:
            conns = list(map(lambda line: line.split(' ')[:3], file_lines[first:last]))
            for con in conns:
                con[2] = True if con[2] == '+' else False
        if len(conns) == 0:
            logging.info(f'{prj_name} not found in fix_conns.')        
    except IOError:
        logging.exception('')
    finally:
        return conns


def local_backup(data_folder: Path, backup_dir: Path) -> bool:
    files_to_copy = get_las_files(data_folder)
    try:
        if not os.path.isdir(backup_dir):
            delivers.check_folder_exists(backup_dir.absolute())
        for file in tqdm(files_to_copy, 
                         desc='Copying Twrs & Cables data to local folder', 
                         total=len(files_to_copy)):
            if Path(backup_dir / file.name).is_file():
                continue
            las_file  = laspy.read(file)
            class_arr = las_file.raw_classification
            new_pts   = las_file.points[np.logical_or(class_arr == 15, 
                                                      class_arr == 16)].array
            new_pts   = copy.deepcopy(new_pts)
            new_las   = _new_las_data(new_pts, las_file.header)
            new_las.write(f'{backup_dir}\\{file.name}')
    except:
        logging.exception('')
        return False
    else:
        return True

def _get_pts(numpy_pts: np.ndarray, las_file: LasData, las_class, sample) -> np.ndarray:
    try:
        new_pts = las_file.points[las_file.raw_classification == las_class].array[:-1:sample]
        new_pts = copy.deepcopy(new_pts)
        return new_pts if numpy_pts.size == 0 else np.append(numpy_pts, new_pts)
    except Exception as e:
        logging.exception('')
        

def _new_las_data(pts_array: np.ndarray, las_header: laspy.LasHeader) -> LasData:
    new_las = laspy.create(point_format=las_header.point_format, 
                            file_version=las_header.version)
    new_las.header = las_header
    new_las.points = PackedPointRecord(pts_array, las_header.point_format)
    new_las.update_header()
    return new_las


def _get_lines_of_interest(file_lines: list, prj_name: str) -> tuple[int, int]:
    first = -1
    last = -1
    if len(file_lines) == 0:
        return first, last
    for it, line in enumerate(file_lines):
        if ':' in line:
            code = line.split(':')[0]
            if code == prj_name:
                first = it + 1
                continue
            if first != -1:
                last = it + -1
                break
    if last == -1:
        last = it + 1
    return first, last


def get_las_files(path: Path) -> list:
    files = list(item 
            for  item in path.glob('**/*') 
            if   item.is_file() and 'las' in item.suffix)  
    if files == []:
        raise IOError(f'Data folder is empty: {path.anchor}')
    return files