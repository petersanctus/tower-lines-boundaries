from copy import copy
from genericpath import isdir
from typing import Iterator
from plboundaries.jobs.search_copy_spandirs import Copy_spans
from plboundaries.project import Project
from plboundaries.tools import delivers
from plboundaries.tools import missions_tools as etools
import logging as log
import os
import shutil
from pathlib import Path
import copy

class ERedes_Process:
    def __init__(self) -> None:        
        self._filter_years = []
        self._dist_cod     = (' ',' ')
        self._conc_cod     = (' ',' ')
        self._work_folder  = ''
        self._missions_to_process = {}
        self._data_folders = {
                f'{year}':f'\\\\flautim\\E-Redes_{year}\\dados_originais\\{year}' 
            for year in range(2020, 2015, -1)}
        self._config_options = {
            'work_folder'       : self.work_folder,
            'data_folder'       : list(self.data_folders.values())[0],
            'sub_produtos'      : Path(rf'\\FLAUTIM\SubProdutos'),
            'skip files to crop': 0,
            'sample_step'       : 5,
            'twr_names_file'    : f'{self.conc_cod[1]}_Twrs.txt',
            'fix_conns_file'    : 'fix_conns.txt',
            'prefix'            : f'{self.dist_cod[0]}_{self.conc_cod[0]}_P.'
            }
        self._process_options = {
            'search_buffer_cluster_twrs' : 1,
            'transv_buff'                : 25,
            'long_buff'                  : 10,
            'search_connec_twr_radius'   : 600,
            'lat_buff_to_check_twr_b2win': 3.5,
            'search_cable_radius'        : 1.9,
            'crop_files'                 : False,
            'process_all_years'          : True,
            'replace_twrs_file'          : 'n'
            }

    @property
    def filter_years(self) -> list[str]:
        return self._filter_years

    # distric code and initials, ex: ('05', 'CB')
    @property
    def dist_cod(self) -> tuple[str, str]:
        return self._dist_cod
    
    # county code and initials, ex: ('06', 'Gois')
    @property
    def conc_cod(self) -> tuple[str, str]:
        return self._conc_cod    
    
    # working folder, where all the process data will be temporarily saved
    @property
    def work_folder(self) -> str:
        return (self._work_folder + rf'\{self.dist_cod[0]}'
                rf'-{self.conc_cod[0]}-{self.conc_cod[1]}')
        
    @property
    def data_folders(self) -> dict[str, str]:
        if self.filter_years == []:
            return self._data_folders
        else:
            return dict(filter(lambda e: e[0] in self.filter_years, 
                                         self._data_folders.items()))

    # options regarding data folders, county, distric and general options
    @property
    def config_options(self) -> dict:
        self._config_options['work_folder']    = self.work_folder
        self._config_options['twr_names_file'] = f'{self.conc_cod[1]}_Twrs.txt'
        self._config_options['prefix']         = f'{self.dist_cod[0]}_{self.conc_cod[0]}_P.'
        return self._config_options
    
    # options regarding algorithm process options
    @property
    def process_options(self) -> dict:
        return self._process_options
    
    @property
    def missions_to_process(self) -> dict:
        return self._missions_to_process
    
    @property
    def missions_years(self) -> list[str]:
        return [row[1] for row in list(self.missions_to_process.values())]
        
    @work_folder.setter
    def work_folder(self, path: str):
        self._work_folder = path
    
    @data_folders.setter
    def data_folders(self, data_folders: dict):
        self._data_folders = data_folders

    @filter_years.setter
    def filter_years(self, filter: list[str]):
        self._filter_years = filter

    @dist_cod.setter
    def dist_cod(self, dist_cod: tuple[str, str]):
        self._dist_cod = dist_cod

    @conc_cod.setter
    def conc_cod(self, conc_cod: tuple[str, str]):
        self._conc_cod = conc_cod

    @missions_to_process.setter
    def missions_to_process(self, missions_to_process: dict):
        self._missions_to_process = missions_to_process
  

    def part_0(self) -> None:
        delivers.check_folder_exists(self.config_options["work_folder"])
        self._create_initial_files()
        for data_folder in self.data_folders.values():
            for mission_folder in os.listdir(data_folder):
                if mission_folder in self.missions_to_process.keys():
                    _report_las_in_dirs(
                        Path(f'{data_folder}\\{mission_folder}'),
                        f'{self.config_options["work_folder"]}',
                        self.missions_to_process[mission_folder][0])   


    def part_1(self) -> None:
        with open(
            f'{self.config_options["work_folder"]}\\spans_kms.txt', 'w') as spans_out:
                spans_out.write('misson kms #span\n')
        data_folders = self.data_part1()
        for year, data_folder in data_folders.items():                
            if int(year) in self.missions_years:
                self.config_options['data_folder'] = data_folder
                _crop_process(
                    self.config_options, 
                    self.process_options, 
                    self.missions_to_process)        


    def part_2(self) -> None:
        _start_logg(self.config_options)
        print('\nPart_2 **************************')
        dest_paths = []
        missions_code = [item[0] for item in self.missions_to_process.values()]
        for item in _items_to_copy(self.config_options, missions_code):
            if os.path.isdir(f'{self.config_options["work_folder"]}\\{item}'):
                dest_path = self._dados_separados_path(_year(item))
                if not dest_path in dest_paths:
                    dest_paths.append(dest_path)
                _copy_mission_dir(self.config_options, dest_path / item)
            else:
                _copy_txt_files(self.config_options, item, dest_paths)


    def part_3(self) -> None:
        _start_logg(self.config_options)
        print('\nPart_3 **************************')
        folders_to_process = self._data_part3()
        destination_folder = self.config_options['sub_produtos'] / self.dist_cod[0]
        copy_job = Copy_spans(folders_to_process, destination_folder)
        copy_job.copy_spans()


    @classmethod
    def deepcopy(cls, options) -> dict:
        return copy.deepcopy(options)


    def _create_initial_files(self):
        list_of_las_files = rf'{self.config_options["work_folder"]}\list_of_las_files.txt'
        twrs_file   = rf'{self.config_options["work_folder"]}\{self.conc_cod[1]}_Twrs.txt'
        fix_coons   = rf'{self.config_options["work_folder"]}\fix_conns.txt'
        with open(list_of_las_files, 'w') as las_list_IO:
            las_list_IO.write('')
        if not os.path.isfile(twrs_file): # check if file already exists
            with open(twrs_file, 'w') as prj_twr:
                prj_twr.write('')
        if not os.path.isfile(fix_coons): # check if file already exists
            with open(fix_coons, 'w') as fix_conns:
                fix_conns.write('')

    
    def data_part1(self) -> dict:
        single = {self.config_options['data_folder'].split('\\')[-1]: 
                  self.config_options['data_folder']}
        return ( self.data_folders 
            if   self.process_options['process_all_years']
            else single)


    def _data_part3(self) -> list[Path]:
        for year in self.data_folders.keys():
            if self._dados_separados_path(year).exists():
                yield self._dados_separados_path(year)


    def _dados_separados_path(self, year: str) -> Path:
        return Path(
            rf'\\flautim\E-Redes_{year}\dados_separados'
            rf'\{self.dist_cod[0]} - {self.dist_cod[1]}'
            rf'\{self.conc_cod[0]} - {self.conc_cod[1]}')


def _crop_process(
    config_options: dict, process_options: dict, missions_to_process: dict) -> None:
        _initialize_process(config_options, process_options)
        for mission_folder in _missions_paths(config_options, missions_to_process):
            proc_options = {**config_options, **missions_to_process[mission_folder][2]}
            print(f'\nProcessing {missions_to_process[mission_folder][0]}:\n')
            e_prj = Project(
                work_folder = proc_options['work_folder'],
                prj_name    = f'{missions_to_process[mission_folder][0]}',
                year        = missions_to_process[mission_folder][1],
                data_folder = Path(proc_options["data_folder"]) / mission_folder)
            etools.crop_macro(e_prj, proc_options)
            delivers.output_span_info(e_prj, f'{e_prj.work_folder}\\spans_kms.txt')


def _initialize_process(config_options, process_options) -> None:
    _start_logg(config_options)
    if _user_input(process_options, config_options) == 'y':
        shutil.copyfile(
            f'{config_options["work_folder"]}\\merged_twr_file.txt',
            f'{config_options["work_folder"]}\\{config_options["twr_names_file"]}')
    with (
        open(f'{config_options["work_folder"]}\\'
                f'new_tower_names.txt', 'w') as nt_file,
        open(f'{config_options["work_folder"]}\\'
                f'merged_twr_file.txt', 'w') as mt_file):            
            nt_file.write('')
            mt_file.write('')


def _start_logg(config_options: dict) -> None:
    log.basicConfig(
        filename=f'{config_options["work_folder"]}\\tlboundaries.log',
        level=log.INFO,
        format='%(asctime)s %(levelname)s: %(message)s -- %(module)s>>%(funcName)s')


def _user_input(process_options: dict, config_options: dict) -> str:
        ans       = process_options['replace_twrs_file']
        twr_names = config_options["twr_names_file"]
        while (ans != 'y' and ans != 'n'):
            print(f'\nReplace merged_twr_file into {twr_names} file? (y/n)')
            ans = input()
        return ans


def _copy_mission_dir(config_options: dict, dest_path: Path) -> None:
    if not os.path.exists(dest_path):
        mission_path = f'{config_options["work_folder"]}\\{dest_path.stem}'            
        print(f'Copying: {dest_path.stem}')                
        shutil.copytree(mission_path, dest_path)   
    else:
        print(f'{dest_path.stem} wasn\'t copied. Already exists in destination folder.')
        log.info(f'{dest_path.stem} wasn\'t copied. Already exists in destination folder.')


def _items_to_copy(config_options: dict, missions_code) -> Iterator[str]:
    return (item 
        for item in os.listdir(config_options['work_folder'])
        if  'twrs' in item.lower() or 'fix' in item.lower() or item in missions_code)  


def _year(item: str) -> str:
    return f'20{item[:2]}'


def _copy_txt_files(config_options: dict, item: str, dest_paths: list[str]) -> None:
        if 'twrs' in item.lower() or 'fix' in item.lower():
            file_path = f'{config_options["work_folder"]}\\{item}'
            print(f'Copying: {item}')
            for path in dest_paths:                    
                shutil.copy(file_path, path)


def _report_las_in_dirs(data_path: Path, workfolder: str, mission_name: str, ) -> None:
    try:
        files_in_path = data_path.glob('**/*')
        files = list(item for item in files_in_path if \
                        item.is_file() and 'las' in item.suffix)
        _print_las_list(files, workfolder, mission_name)
    except Exception:
        raise


def _print_las_list(files, workfolder, mission_name) -> None:
    with open(f'{workfolder}\\list_of_las_files.txt', 'a') as las_list_IO:
        las_list_IO.write(f'Ficheiros da missão {mission_name}:\n')
        delivers.print_list(las_list_IO, files)
        las_list_IO.write('\n')


def _missions_paths(config_options: dict, missions_to_process: dict):
    return (mission 
        for mission in os.listdir(config_options['data_folder'])
        if  mission in missions_to_process.keys())


if __name__ == "__main__":
    erp = ERedes_Process()
    assert _year('1606406') == '2016'
    assert _year('1706406') == '2017'
    assert _year('1806406') == '2018'
    assert _year('1906406') == '2019'
    assert _year('2006406') == '2020'
    assert _year('2006406') != '2019'