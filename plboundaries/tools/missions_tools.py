from plboundaries.project import Project
from plboundaries.tools import delivers
import time
import logging as log
import os
import shutil
from pathlib import Path

### PROCESS TOOLS
# Tools to help the scrip to process several missions of several years, for examples the missions
# of a county
#
# These tools are not used in the plboundaries package, only for mission's script porpuses 
###

def crop_macro(crop_prj: Project, proc_options):
    start = time.perf_counter()
    log.info(f'++++ Crop Macro initiated on project: {crop_prj.prj_name}:')
    crop_prj.ext_twr_names = f'{proc_options["work_folder"]}\\{proc_options["twr_names_file"]}'
    crop_prj.import_twrs_and_cables()
    crop_prj.identify_towers(search_buffer = proc_options["search_buffer_cluster_twrs"])
    crop_prj.find_connections(proc_options)
    crop_prj.rename_twrs(prefix=proc_options['prefix'])
    crop_prj.output_twrs_to_txt()
    crop_prj.output_to_dxf()
    crop_prj.output_twrs_and_cables_las()
    if proc_options['crop_files']:
        crop_prj.crop_las_to_span_dirs(sampling=proc_options['sample_step'], 
                                       skip=proc_options['skip files to crop'])
    crop_prj.merge_twr_names_files()
    finish = time.perf_counter()
    log.info(f'----------------------Crop Macro finished in {round(finish-start,2)} second(s)')
    return crop_prj.twr_ident.count


def correct_rn_macro(line_test: Project):
    start = time.perf_counter()
    log.info(f'++++ Correct Return Numbers Macro initiated on project: {line_test.prj_name}:')
    line_test.correct_rn(line_test.data_folder, 
                         f'{line_test.data_folder}\\RN_Fixed')
    finish = time.perf_counter()
    log.info(f'----------------------Correct Return Numbers Macro finished in {round(finish-start,2)} second(s)')


def initialize_process(config_potions, process_options):
    log.basicConfig(filename=f'{config_potions["work_folder"]}\\tlboundaries.log',
                                    level=log.INFO,
                                    format='%(asctime)s %(levelname)s: %(message)s -- %(module)s>>%(funcName)s')
    replace = process_options['replace_twrs_file']
    while replace != 'y' and replace != 'n':
        print(f'\nReplace merged_twr_file into {config_potions["twr_names_file"]} file? (y/n)')
        replace = input()
    if replace == 'y':
        shutil.copyfile(f'{config_potions["work_folder"]}\\merged_twr_file.txt',
                        f'{config_potions["work_folder"]}\\{config_potions["twr_names_file"]}')
    with open(f'{config_potions["work_folder"]}\\new_tower_names.txt', 'w') as nt_file,\
         open(f'{config_potions["work_folder"]}\\merged_twr_file.txt', 'w') as mt_file:            
            nt_file.write('')
            mt_file.write('')

def check_las_files_in_dirs(path, workfolder, mission_name, skip=0) -> None:
        try:
            path = Path(path)
            files_in_path = path.glob('**/*')
            files = list(item 
                     for item in files_in_path 
                     if  item.is_file() and 'las' in item.suffix)[skip:]
            _print_las_files_list(files, workfolder, mission_name)
        except Exception:
            raise

def _print_las_files_list(files, workfolder, mission_name):
    with open(f'{workfolder}\\list_of_las_files.txt', 'a') as las_list_IO:
        las_list_IO.write(f'Ficheiros da missão {mission_name}:\n')
        delivers.print_list(las_list_IO, files)
        las_list_IO.write('\n')