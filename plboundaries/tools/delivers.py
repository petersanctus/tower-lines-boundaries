from plboundaries.baseclasses.point import Point2D
from plboundaries.jobs.crop_to_span import Crop_Las
from ezdxf.layouts.layout import Modelspace
from pandas.core.frame import DataFrame
from io import TextIOWrapper
import ezdxf
import logging
import os
import numpy as np
import laspy

def output_twrs_to_txt(project, output_folder):
    twr_ident   = project.twr_ident
    spans_found = project.spans_found
    year        = project.year
    with open(f'{output_folder}\\tower_group_pts_{year}.txt', 'w') as tg_out, \
         open(f'{output_folder}\\centroids_{year}.txt', 'w', newline = '\n') as c_out, \
         open(f'{output_folder}\\towers_{year}.txt', 'w', newline = '\n') as t_out, \
         open(project.older_twr_names, 'a', newline = '\n') as t_stack_out:
        twr_ident.print_towers_groups(tg_out)
        twr_ident.print_towers_centroid(c_out)
        print_list(t_out, _get_towers_wo_duplicates(spans_found))
        print_list(t_stack_out, _get_towers_wo_duplicates(spans_found))


def output_to_dxf(project, output_folder):
    spans_found  = project.spans_found
    twr_centroid = project.twr_ident.towers_centroid
    year         = project.year
    try:            
        logging.getLogger().setLevel(logging.WARNING)
        doc = _initialize_dxf()
        msp = doc.modelspace()
        twrs = _get_towers_wo_duplicates(spans_found)
        _insert_centroids_in_dxf(msp, twr_centroid)
        _insert_towers_in_dxf(msp, twrs, layer_name='Twrs')
        _insert_towers_in_dxf(msp, project.twrs_wout_cons, layer_name='Twrs_no_cons')
        _insert_towers_in_dxf(msp, project.twrs_renamed, layer_name='Twrs_renamed')
        _insert_spans_in_dxf(msp, spans_found)
        doc.saveas(f'{output_folder}\{project.prj_name}_spans_{year}.dxf')
        logging.getLogger().setLevel(logging.INFO)      
    except Exception as e:
        logging.exception(' ')
        logging.getLogger().setLevel(logging.INFO)
    finally:
        pass


def crop_las_data(seeder: Crop_Las, files_to_crop: list):
    print(f'\n--------------------------\nCroping las files to span:')
    logging.info(f'Sample step defined to: 1:{seeder.sample_step}')
    logging.info(f'Cropping and delievering span files. Files to process: {len(files_to_crop)}.')
    for it, file in enumerate(files_to_crop):        
        print(f'\nCropping {file.stem}.las {it+1}/{len(files_to_crop)}:')        
        seeder.crop_to_spans(file)        
    logging.info(f'Cropped {it} las files to txt spans.')


def check_folder_exists(dir: str):
    try:
        if not os.path.isdir(f"{dir}"):
            os.makedirs(f"{dir}")
    except Exception:
        logging.exception('')


def output_tc_las(project, output_folder):
    twrs = project.twr_las_data
    cables = project.cables_las_data
    cables.write(f'{output_folder}\{project.prj_name}_cables.las')
    twrs.write(f'{output_folder}\{project.prj_name}_twrs.las')
    
    
def output_spans(project, output_folder):
    dists = []
    with open(f'{output_folder}\\spans_{project.year}.txt', 'w') as spans_out:
        spans_out.write(f'Spans da missão {project.prj_name} e km associados:\n')
        for span in project.spans_found:
            span_dist = np.linalg.norm(span.v12)/1000 # m => km
            dists.append(span_dist)
            spans_out.write(f'{span.id} {span_dist:.3f}\n')
        spans_out.write(f'\nTotal de km: {np.sum(dists):.3f}')
        spans_out.write(f'\nMedia      : {np.mean(dists):.3f}')
        spans_out.write(f'\nstdv       : {np.std(dists):.3f}')
        spans_out.write(f'\nMax        : {np.max(dists):.3f}')
        spans_out.write(f'\nMin        : {np.min(dists):.3f}')


def output_span_info(project, output_folder):
    dists = []
    for it, span in enumerate(project.spans_found):
        span_dist = np.linalg.norm(span.v12)/1000 # m => km
        dists.append(span_dist)
    with open(output_folder, 'a', newline = '\n') as spans_out:
        spans_out.write(f'{project.prj_name}: {np.sum(dists):.1f} {it+1}\n')


def print_list(f_out: TextIOWrapper, data_list: list):
    try:  
        data_list.sort()
        for item in data_list:
            f_out.write(f'{item}\n')
    except Exception:
        logging.exception('')


# a list of the twrs where each twr only appears once in the list
def _get_towers_wo_duplicates(spans_found: list) -> list[Point2D]:
    twrs = _get_towers_w_duplicates(spans_found)
    twrs  = [twr for n, twr in enumerate(twrs) if twr not in twrs[:n]]
    return twrs


# gets a list of twrs with duplicates. Because spans share twrs with other spans
# a list of all spans's twr_1 and twr_2 can have twrs duplicated in several positions of the list
def _get_towers_w_duplicates(spans_found: list) -> list:
    twrs = []
    [twrs.extend([span.tower_1, span.tower_2]) for span in spans_found]
    return twrs


def _initialize_dxf():
    doc = ezdxf.new('R2010')  #  DXF version name: 'AC1024'
    doc.layers.new(name='Spans',        dxfattribs={'color': 30})
    doc.layers.new(name='Twrs',         dxfattribs={'color': 3,  'lineweight': 90})
    doc.layers.new(name='Twrs_no_cons', dxfattribs={'color': 4,  'lineweight': 90})
    doc.layers.new(name='Twrs_renamed', dxfattribs={'color': 7,  'lineweight': 90})
    doc.layers.new(name='Centroids',    dxfattribs={'color': 6, 'lineweight': 90})
    doc.layers.new(name='Twrs_Text',    dxfattribs={'color': 2})
    doc.layers.new(name='Centr_Text',   dxfattribs={'color': 2})
    doc.layers.new(name='Conns',        dxfattribs={'color': 131})
    return doc


def _insert_centroids_in_dxf(msp: Modelspace, twr_centroid: DataFrame):
    for id, m, p, h in zip(list(twr_centroid.index),
                           list(twr_centroid['m']), 
                           list(twr_centroid['p']),
                           list(twr_centroid['h'])):
        msp.add_point((m, p, h), dxfattribs={'layer': 'Centroids'})
        msp.add_text(id, dxfattribs={'layer': 'Centr_Text', 'height':3}). \
                     set_pos((m+5, p+4), align='CENTER')


def _insert_towers_in_dxf(msp: Modelspace, twrs: list, layer_name=''):
    for twr in twrs:
        msp.add_point((twr.x, twr.y), dxfattribs={'layer': layer_name})
        if layer_name == 'Twrs':
            msp.add_text(twr.id, dxfattribs={'layer': 'Twrs_Text', 'height':3}). \
                     set_pos((twr.x+5, twr.y+4), align='CENTER')


def _insert_spans_in_dxf(msp: Modelspace, spans_found: DataFrame):
    for span in spans_found:
        t1 = span.tower_1
        t2 = span.tower_2
        boundary = span.boundary.exterior.coords
        if boundary:
            msp.add_polyline2d(boundary,  dxfattribs={'layer': 'Spans'})          
        msp.add_line((t1.x, t1.y), (t2.x, t2.y), dxfattribs={'layer': 'Conns'})