from plboundaries.baseclasses.point import Point2D
from plboundaries.jobs import fix_las as fl
from scipy.spatial import cKDTree
from plboundaries.tools import delivers
import laspy
import numpy as np
import copy
import logging
import os

def rename_twrs(project) -> list:
    spans_to_change  = copy.deepcopy(project.spans_found)
    try:                
        new_names       = _get_towers_w_duplicates(spans_to_change)
        twrs_to_check   = _get_ext_and_older_names(project)
        if twrs_to_check:
            _rename_twrs(new_names, project.twrs_renamed, twrs_to_check)            
        logging.info(f'Renamed {len(project.twrs_renamed)} tower(s).')
    except Exception:
        logging.exception('')
    finally:
        return spans_to_change


def add_sufix_prefix_to_twrs_id(project) -> list:
    spans_to_change = copy.deepcopy(project.spans_found)
    try:        
        twrs_old_names  = _get_towers_w_duplicates(spans_to_change)
        for twr in twrs_old_names:
            if project.twrs_prefix != '':
                twr.id = f'{project.twrs_prefix}{twr.id}' if \
                         not project.twrs_prefix in str(twr.id) else \
                         twr.id
            if project.twrs_sufix != '':
                twr.id = f'{twr.id }{project.twrs_sufix}'  if \
                         not project.twrs_sufix in str(twr.id) else \
                         twr.id
    except Exception:
        logging.exception('')
    finally:
        return spans_to_change


def correct_rn(files_to_crop: list, output_folder):
    try:
        logging.info(f'Correcting return numbers:')        
        for it, file in enumerate(files_to_crop):
            logging.info(f'Processing {file.stem}.las {it+1}/{len(files_to_crop)}')
            las_file = laspy.read(file)
            logging.info(f'Total # of points: {len(las_file.points.array)}')
            las_new = fl.fix_las_rn(las_file)
            las_new.write(output_folder + f'\{file.stem}_rn_fixed.las')
    except Exception as e:
        logging.exception('')


def merge_twr_names_files(project):
    twrs_ext = _get_ext_and_older_names(project)
    with open(f'{project.work_folder}\\merged_twr_file.txt', 'w', newline = '\n') as t_merged:
        delivers.print_list(t_merged, twrs_ext)
    

def _rename_twrs(twrs_new_names, twrs_renamed, older_twr_names):    
    twrs_not_renamed = []      
    twrs_qt          = _2dtree_of(older_twr_names)
    for twr in twrs_new_names:
        idx_new_name = _get_index_of_near_twr(twr, twrs_qt, older_twr_names)
        if idx_new_name:
            twr.id = older_twr_names[idx_new_name[0]].id
            if not twr in twrs_renamed:
                twrs_renamed.append(twr)
        else:
            twrs_not_renamed.append(twr)
    twrs_not_renamed.sort()
    _check_twrs_duplicated_ids(twrs_not_renamed, older_twr_names)
    

def _2dtree_of(data: list) -> cKDTree:
    x = list(map(lambda item: item.x, data))
    y = list(map(lambda item: item.y, data))
    data_xy = np.array([x, y]).transpose()
    return cKDTree(data_xy)


def _get_names(ext_twr_names: str) -> list:
    new_names = []
    if not os.path.isfile(ext_twr_names):
        return new_names
    try:
        with open(ext_twr_names, 'r') as ftwr_names:
            new_names = list(map(lambda line: Point2D(float(line[1]), float(line[2]), line[0]),
                                 map(lambda line: line.split(' ')[:3], 
                                     ftwr_names.read().splitlines())))
        if len(new_names) == 0:
            logging.warning(f'External file is empty: {ext_twr_names}')
    except IOError:
        logging.exception('')
    finally:
        return new_names

# gets a list of twrs with duplicates. Because spans share twrs with other spans
# a list of all spans's twr_1 and twr_2 can have twrs duplicated in several positions of the list
def _get_towers_w_duplicates(spans_found: list) -> list:
    twrs = []
    [twrs.extend([span.tower_1, span.tower_2]) for span in spans_found]
    return twrs


def _get_index_of_near_twr(twr, twrs_qt, twrs_new_names):
    search_range = 3.5
    idx_new_name = twrs_qt.query_ball_point(np.array([twr.x, twr.y]), search_range)
    while len(idx_new_name) > 1:
        search_range -= 1
        idx_new_name = twrs_qt.query_ball_point(np.array([twr.x, twr.y]), search_range)
        if search_range < 0:
            twr_in_same_loc = ' '.join([twrs_new_names[idx].id for idx in idx_new_name])
            logging.warning(f'There is {len(idx_new_name)} in the same location, twrs: {twr_in_same_loc}')
            return idx_new_name
    return idx_new_name


def _check_twrs_duplicated_ids(twrs_not_renamed:list[Point2D], twrs_new_names:list):
    twrs_old_ids = {}    
    new_ids      = [twr.id for twr in twrs_new_names]
    for twr_not_renamed in twrs_not_renamed:
        if str(twr_not_renamed.id) in new_ids:
            if twr_not_renamed.id in twrs_old_ids:
                twr_not_renamed.id = twrs_old_ids[twr_not_renamed.id].id
                continue
            twrs_old_ids[copy.deepcopy(twr_not_renamed.id)] = twr_not_renamed
            _assign_available_id(twr_not_renamed, new_ids)
            new_ids.append(twr_not_renamed.id)


def _assign_available_id(twr_not_renamed: list, new_ids: list):
    while str(twr_not_renamed.id) in new_ids:
        if str(twr_not_renamed.id).isdigit():
            twr_not_renamed.id = str(int(twr_not_renamed.id) + 1)
        elif str(twr_not_renamed.id.split('.')[-1]).isdigit() and \
           len(twr_not_renamed.id.split('.')) == 2:            
             part_str = twr_not_renamed.id.split('.')[0]
             part_int = int(twr_not_renamed.id.split('.')[1]) + 1
             twr_not_renamed.id = f'{part_str}.{part_int}'
        else:
            twr_not_renamed.id += '_1'


def _get_ext_and_older_names(project) -> list:
    twrs_ext_names  = _get_names(project.ext_twr_names)
    older_twr_names = _get_names(project.older_twr_names)
    for twr_old in older_twr_names:
        if not twr_old in twrs_ext_names:
            twrs_ext_names.append(twr_old)
    return twrs_ext_names