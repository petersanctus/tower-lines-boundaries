from io import TextIOWrapper
import logging
import laspy
from scipy.spatial import cKDTree
from plboundaries.baseclasses.point import *
from plboundaries.baseclasses.span import Span
import numpy as np
from tqdm import tqdm
import os
import concurrent.futures
from shapely.geometry import Point
import copy

RETURN_NUMBER_MASK_0 = laspy.point.dims.RETURN_NUMBER_MASK_0
NUMBER_OF_RETURNS_MASK_0 = laspy.point.dims.NUMBER_OF_RETURNS_MASK_0
CLASSIFICATION_MASK_0 = laspy.point.dims.CLASSIFICATION_MASK_0

class Crop_Las():

    def __init__(self, project, sample_step = 5) -> None:
        self._spans_found = project.spans_found
        self._spans_f_qt = self._2dtree_of(project.spans_found)
        self._opened_files = []
        self._output_folder = project.output_folder
        self._sample_step = sample_step
        self._year = project.year
        self._biggest_span_dist = self._get_biggest_span_dist()
        


    @property
    def spans_found(self):
        return self._spans_found
    
    @property
    def spans_f_qt(self):
        return self._spans_f_qt
    
    @property
    def cbl_pts(self):
        return self._cbl_pts
    
    @property
    def twr_pts(self):
        return self._twr_pts
    
    @property
    def rem_pts_divided(self):
        return self._rem_pts_divided
    
    @property
    def year(self):
        return self._year
    
    @property
    def sample_step(self):
        return self._sample_step

    @property
    def scale(self) -> int:
        return self._scale
    
    @property
    def biggest_span_dist(self) -> float:
        return self._biggest_span_dist


    def crop_to_spans(self, lasfile: str):
        #class_arr     = las_file.raw_classification        
        with laspy.open(lasfile) as las_reader: 
            self._scale  = las_reader.header.x_scale
            for points in las_reader.chunk_iterator(int(3*10**7)):
                self._cbl_pts, self._twr_pts, rem_pts = self.get_interest_points(points)
                cpu_avalable = os.cpu_count() - 1
                devision = cpu_avalable if cpu_avalable < len(rem_pts) // 250000 else \
                                      1 if len(rem_pts) // 250000 < 1 else \
                                        len(rem_pts) // 250000
                self._rem_pts_divided = np.array_split(rem_pts, devision)
                sucess = self._crop_to_span()
                if sucess:
                    self.output_pts_to_span_dirs()


    def _crop_to_span(self) -> bool: 
        try:
            cbl_pts = self.cbl_pts
            twr_pts = self.twr_pts
            rem_pts_l = self.rem_pts_divided
            with concurrent.futures.ProcessPoolExecutor() as executer:
                results_rem = executer.map(self._pts_to_spans, rem_pts_l)
                results_cbl = executer.submit(self._pts_to_spans, cbl_pts)
                results_twr = executer.submit(self._pts_to_spans, twr_pts)
            self._results = [list(results_rem), results_cbl.result(), results_twr.result()]
        except:
            logging.exception('')
        else:
            return True


    def output_pts_to_span_dirs(self):
        cbl_pts = self.cbl_pts
        twr_pts = self.twr_pts
        for results, pts in zip(self._results[0], self.rem_pts_divided):
            self._output_pts_spandirs(results, pts)
        self._output_pts_spandirs(self._results[1], cbl_pts)
        self._output_pts_spandirs(self._results[2], twr_pts)


    def output_pts_to_one_dir(self):
        cbl_pts = self.cbl_pts
        twr_pts = self.twr_pts
        for results, pts in zip(self._results[0], self.rem_pts_divided):
            self._output_pts_onedir(results, pts)
        self._output_pts_onedir(self._results[1], cbl_pts)
        self._output_pts_onedir(self._results[2], twr_pts)


    def _check_output_folder_exists(self, output_folder):
        if not os.path.isdir(f"{output_folder}"):
            os.makedirs(f"{output_folder}")


    def _output_pts_spandirs(self, spans_data, pts):
        for span_name in tqdm(spans_data.keys(), desc='Output: '):
            self._check_output_folder_exists(f'{self._output_folder}\Spans\{span_name}')
            out_file = f'{self._output_folder}\Spans\{span_name}\{self.year}.txt'
            self._output_pts(span_name, spans_data, pts, out_file)
            

    def _output_pts_onedir(self, spans_data, pts):
        self._check_output_folder_exists(f'{self._output_folder}\Spans')
        for span_name in tqdm(spans_data.keys(), desc='Output: '):
            out_file = f'{self._output_folder}\Spans\{span_name}.txt'
            self._output_pts(span_name, spans_data, pts, out_file)


    def _2dtree_of(self, data: list):
        x = list(map(lambda item: item.centroid.x, data))
        y = list(map(lambda item: item.centroid.y, data))
        data_xy = np.array([x, y]).transpose()
        return cKDTree(data_xy)

    # determine a list of indexes that contains the point
    def _idxs_of_spans_containing(self, line, spans_found: list[Span]) -> list:
        pt = Point(line['X']*self.scale, line['Y']*self.scale)        
        np_pt = np.array([line['X']*self.scale, line['Y']*self.scale])
        near_spans_idxs = self.spans_f_qt.query_ball_point(np_pt, self.biggest_span_dist/2)
        return list(filter(lambda idx: spans_found[idx].contains(pt), near_spans_idxs))


    # calculates the distance of the pt to the towers that defines the span
    def _distance_to_span_towers(self, line, selected_span):
        pt  = np.array([line['X']*self.scale, line['Y']*self.scale])
        t1  = np.array([selected_span.tower_1.x, selected_span.tower_1.y])
        t2  = np.array([selected_span.tower_2.x, selected_span.tower_2.y])
        v1p = t1-pt
        v2p = t2-pt
        return [np.dot(v1p, v1p), np.dot(v2p, v2p)]


    def _rectify_class(self, las_pt, selected_span):
        # class  5 => high_veg
        # class 12 => high_veg key pts
        # class 14 => tower extra, i.e, not a span tower
        # class 15 => tower
        if las_pt['raw_classification'] == 12:
            return 5
        if las_pt['raw_classification'] == 15:
            sq_dists = self._distance_to_span_towers(las_pt, selected_span)
            sq_buff  = 6*6        
            if sq_dists[0] > sq_buff and sq_dists[1] > sq_buff:
                return 14
        return las_pt['raw_classification']


    # return a dict that relates the spans ids with the idxs, in the array, of las pts that belongs to them
    # de dict also stores the value of the output classification
    # dict: keys = span_ids; values = [idx_of_array, pt_las_class]
    def _pts_to_spans(self, pts) -> dict:
        span_id__idx_clss = {}
        spans_found = self.spans_found 
        for it, las_pt in enumerate(tqdm(pts, desc='Divide: ', total=len(pts))):
            idxs = self._idxs_of_spans_containing(las_pt, spans_found)        
            for idx in idxs:
                clss = self._rectify_class(las_pt, spans_found[idx])
                span_id = spans_found[idx].id
                if (span_id in span_id__idx_clss):
                    span_id__idx_clss[span_id].append([it, clss])
                else:
                    span_id__idx_clss[span_id] = [[it, clss]]
        return span_id__idx_clss
    

    def _output_pts(self, span_name, spans_data, pts, out_file):
        try:  
            if not span_name in self._opened_files:
                self._opened_files.append(span_name)
                with open(f"{out_file}", 'w') as f:        
                    f.write("")
            with open(f"{out_file}", 'a') as f_out:
                rows = []
                for it, data in enumerate(spans_data[span_name]):
                    idx = data[0]
                    classification = data[1]
                    x = pts[idx]['X']*0.01
                    y = pts[idx]['Y']*0.01
                    z = pts[idx]['Z']*0.01
                    c = classification & CLASSIFICATION_MASK_0
                    t = pts[idx]['gps_time']
                    i = pts[idx]['intensity']
                    rn = pts[idx]['bit_fields'] & RETURN_NUMBER_MASK_0
                    nr = (pts[idx]['bit_fields'] & NUMBER_OF_RETURNS_MASK_0) >> 3
                    rows.append(f'{x:.2f} {y:.2f} {z:.2f} {c} {t} {i} {rn} {nr} \n')
                f_out.write(''.join(rows))
        except Exception as e:
            logging.error(f'{e.with_traceback}')
            logging.error(f'Error in span: {span_name}')


    # finds the biggest distence b2win 2 towers plus the long buffer of all spans in the prj
    # it ads 2 meters to the value as a margin of error
    def _get_biggest_span_dist(self) -> float:
        biggest_span_dist = 0
        for span in self.spans_found:
            span_dist = np.linalg.norm(span.f - span.c)
            if biggest_span_dist < span_dist:
                biggest_span_dist = span_dist
        return biggest_span_dist + 2


    def _write_lines(self, f_out: TextIOWrapper, rows: list):
        f_out.write(''.join(rows))
        f_out.write('')
        rows[:] = []

    
    def get_interest_points(self, points: laspy.lasreader.LasReader):
        class_arr     = points.array[points.sub_fields_dict['classification'][0]]
        cbl_pts = points[class_arr == 16].array
        twr_pts = points[class_arr == 15].array
        rem_pts       = points[np.logical_and(
                            np.logical_and(class_arr != 7, 
                                            class_arr != 15), 
                                            class_arr != 16)].array
        return cbl_pts, twr_pts, copy.deepcopy(rem_pts[:-1:self.sample_step])