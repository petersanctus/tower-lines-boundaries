from pathlib import Path
from numpy import invert
from plboundaries.tools import delivers
import os
import shutil
import logging as log
import re

class Copy_spans:

    def __init__(self, folders_to_process: list[Path], destination_folder: Path):
        self._folders_to_process = sorted(folders_to_process, reverse=True)
        self._parent_folder = ''
        self._children_folders = ''
        self._destination_folder = destination_folder
        self._parent_spans = {}
        self._spans_unmatched = []


    def children_folders(self, index) -> list[Path]:
        return self._folders_to_process[index+1:]


    def copy_spans(self):
        try:
            child_folder = ''            
            for it, path in enumerate(self._folders_to_process):
                print(f'\n{it+1} / {len(self._folders_to_process)} ' 
                      f'Copying {_year(path)} parents ----------------')
                self._parent_folder = path
                self._copy_parent()
                for itc, child_folder in enumerate(self.children_folders(it)):
                    print(f'\t{itc+1} / {len(self.children_folders(it))} '
                          f'Copying {_year(child_folder)} children')
                    self._copy_child(child_folder)
        except:
            log.exception('')


    def _copy_parent(self):
        for path, _, file in os.walk(self._parent_folder):
            if  Path(path).parts[-2] == 'Spans':
                if self._spans_unmatched == []:
                    self._add_to_parent_spans(path)
                    self._copy_file(path, file[0])
                else:       
                    if self._is_span_to_copy(path.split('\\')[-1]):
                        self._add_to_parent_spans(path)
                        self._copy_file(path, file[0])


    def _copy_child(self, child_folder):        
        for path, _, file in os.walk(child_folder):
            if path.split('\\')[-2] == 'Spans':
                if self._is_parent_span(path):
                    self._copy_file(path, file[0])
                else:
                    self._spans_unmatched.append('\\'.join(path.split('\\')[-3:]))
                    

    # add span to parent list (dict.key) and store destination folder associated (dict.item)
    def _add_to_parent_spans(self, path: str):
        if not path.split('\\')[-1] in self._parent_spans.keys():
            span_dir = '\\'.join(path.split('\\')[-4:])
            self._parent_spans[path.split('\\')[-1]] = f'{self._destination_folder}\\{span_dir}'


    def _is_parent_span(self, path: str) -> bool:
        return path.split('\\')[-1] in self._parent_spans.keys()

    
    def _copy_file(self, path: str, file: str):
        try:
            span = path.split('\\')[-1]
            src_file = f'{path}\\{file}'
            dst = self._parent_spans[span]
            delivers.check_folder_exists(dst)
            shutil.copy(src_file, dst)
        except IOError:
            log.exception('')                  
        finally:
            return

    def _is_span_to_copy(self, span_to_check) -> bool:
        for span in self._spans_unmatched:
            if span_to_check in span:
                return True
        return False

def _year(path: Path) -> str:
    return re.findall(r'\d+', path.anchor)[0]