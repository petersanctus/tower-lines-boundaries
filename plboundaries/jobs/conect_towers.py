from typing import Any
from scipy import stats
import logging
import copy
from ezdxf import math
from numpy.core.records import array
from scipy.spatial import cKDTree
import numpy as np
from plboundaries.baseclasses.point import Point2D
from plboundaries.baseclasses.cables import Cables
from plboundaries.baseclasses.span import Span
from tqdm import tqdm
import pandas as pd
from scipy import stats
import math

class Conect_towers:

    def __init__(self, cables: Cables, towers_c: pd.DataFrame, proc_options: dict):
        self._search_radius = proc_options["search_connec_twr_radius"]
        self._cables = cables
        self._towers_c = towers_c
        if self.c_count == 0:
            logging.warning(r'No towers for connections')
            return
        self._connections = [[0 for i in range(self.c_count)] \
                                for j in range(self.c_count)]        
        self._towers_c_qt         = self._2dtree_of(towers_c)
        self._lat_buff_2_del_con  = proc_options['lat_buff_to_check_twr_b2win']
        self._search_cable_radius = proc_options['search_cable_radius']
    
    @property
    def cables(self) -> Cables:
        return self._cables
        
    @property
    def search_radius(self) -> Cables:
        return self._search_radius

    @property
    def towers_c(self) -> pd.DataFrame:
        return self._towers_c

    @property
    def towers_c_qt(self) -> cKDTree:
        return self._towers_c_qt

    @property
    def c_count(self) -> int:
        return len(self.towers_c)

    # list[][] with #tower rows and collumns, with 'twr' objects as entries when 
    # there are a connection between them, i.e., they are connected with cables
    @property
    def connections(self) -> list:
        return self._connections


    def find_connections(self, fix_conns: dict):
        if self.c_count == 0:
            logging.warning(f'Find connections is not possible due to no tower'
                            f' centroid available')
            return
        for x, y, id in tqdm(zip(self.towers_c['m'],        
                             self.towers_c['p'], 
                             self.towers_c.index),
                             desc='Find tower connections',
                             total=len(self.towers_c)):
            tower_x = Point2D(x, y, id)
            self._find_possible_connections(tower_x, id)
            self._del_false_connections(tower_x)
        self._remove_triagulations()
        self._ext_fix_conns(fix_conns)

    
    def check_towers_with_no_connections(self) -> list:
        twrs_w_no_cons = []
        for twr_idx, twr_cons in enumerate(self.connections):
            if not any(isinstance(value, Point2D) for value in twr_cons):
                twr = Point2D(x=self.towers_c.loc[twr_idx]['m'], 
                              y=self.towers_c.loc[twr_idx]['p'], 
                              id=twr_idx)
                twrs_w_no_cons.append(twr)
        logging.debug(f'Found {len(twrs_w_no_cons)} centroid(s) with no connections.')
        for centroid in twrs_w_no_cons:                 
            logging.debug(f'Centroid {centroid} has no connection.')
        return twrs_w_no_cons


    def print_connections(self):
        all_towers = sorted(self.towers_c.qt_centroids.all_points(), 
                            key=lambda twr: twr.id)
        for twr in all_towers:
            print(f'Tower {twr.id} has connections with:')
            for col in range(0, self.c_count):
                if isinstance(self.connections[twr.id-1][col], Point2D):
                    print(f'- {self.connections[twr.id-1][col].id}')
            print("\n")


    def spans_found(self, proc_options) -> list:
        '''
        Returns a list with spans id as keys and a span as associated value

        Parameters
        ----------
        transv_buff and long_buff: float
            transversal buffer are the extend from the line that connects the 2 
                towers_c to both sides.
            longitudinal buffer are the extend behind and beyound from twr 1 and twr 2

        Returns
        -------
        list
            a list with span objects
        '''
        spans = []
        for m, p, id in zip(self.towers_c['m'], self.towers_c['p'], self.towers_c.index):
            twr = Point2D(m, p, int(id))
            for col in range(twr.id, self.c_count): 
                if isinstance(self.connections[twr.id][col], Point2D):
                    valid_span = Span(copy.deepcopy(twr), 
                                      self.connections[twr.id][col],
                                      proc_options["transv_buff"], 
                                      proc_options["long_buff"])
                    spans.append(valid_span)
        return spans


    def _2dtree_of(self, data: pd.DataFrame):
        m  = list(data['m'])
        p  = list(data['p'])
        data_xy = np.array((m,p)).transpose()
        return cKDTree(data_xy) # type:ignore


    def _find_possible_connections(self, tower: Point2D, twr_id: str): 
        twr_in = np.array([tower.x, tower.y])
        neighbor_twrs = self.towers_c_qt.query_ball_point(twr_in, self.search_radius) #type:ignore
        if len(neighbor_twrs) <= 1:
            logging.warning(f'No towers found in {self.search_radius} m radius '
                            f'of twr {twr_id}, verify length of search radius.')
            return
        for near_tower_idx in neighbor_twrs:
           if not (tower.id == near_tower_idx):
                near_t_m = self.towers_c.iloc[near_tower_idx]['m'] #type:ignore
                near_t_p = self.towers_c.iloc[near_tower_idx]['p'] #type:ignore
                t1 = tower
                t2 = Point2D(near_t_m, near_t_p, near_tower_idx)
                if self._cable_between_towers(t1, t2):
                    self.connections[tower.id][near_tower_idx] = t2
                    self.connections[near_tower_idx][tower.id] = t1


    def _cable_between_towers(self, twr1: Point2D, twr2: Point2D) -> bool:
        try:
            dist_btwin_twrs = np.linalg.norm(np.array([twr1.x, twr1.y]) - \
                                             np.array([twr2.x, twr2.y]))
            zones_count     = max(5, min(15, int(dist_btwin_twrs / self._search_cable_radius)))
            zones           = self._zones_b2win_twrs(twr1, twr2, zones_count)
            zones_pts       = self._fill_zones(zones)
            zones_wo_pts, zones_pt_count = self._count_pts_in_zones(zones_pts)
            sqdists_to_centerline = self._sqdist_to_centerline(zones_pts, twr1, twr2)
            search_area = self._search_cable_radius*self._search_cable_radius*math.pi #type:ignore
            if zones_wo_pts >= int(zones_count/3):
                return False
            if dist_btwin_twrs < 40 and zones_pt_count[int(zones_count/2)] > 5:
                return True
            if stats.variation(zones_pt_count) < 0.25:
                return True
            if min(zones_pt_count)/search_area > 4:
                return True
            if abs(self._angle_b2win(twr1, twr2, zones_pts)) > 1.1:
                return False
            if (math.sqrt(max([dist for dist in sqdists_to_centerline if dist != 9999])) < #type:ignore
             self._search_cable_radius  * 1/3):
                return True
            at_beggin = self._is_gradient(sqdists_to_centerline)
            at_end = self._is_gradient(sqdists_to_centerline[::-1])
            return False if at_beggin or at_end else True
        except Exception as e:
            logging.error(e)
            logging.error(f'twr1: {twr1.id}; twr2: {twr2.id}')
            return False


    def _fill_zones(self, zones: list[np.ndarray]) -> list[np.ndarray]:
        return [self.cables.cable_points[self._idxs(zone)]
            for zone in zones]


    def _idxs(self, zone: np.ndarray) -> list[int]:
        return self.cables.qt_points.query_ball_point( #type:ignore
                zone, 
                self._search_cable_radius)


    def _is_gradient(self, dists_sq) -> bool:
        count = int(len(dists_sq)/3)
        if count <= 3:
            return False
        is_gradient = True
        for it in range(0, count):
            is_gradient = is_gradient and dists_sq[it] >= dists_sq[it + 1]
        return is_gradient


    def _del_false_connections(self, tower_x: Point2D):
        conns_w_tower_x = self._get_conns(tower_x)
        tx  = np.array([tower_x.x, tower_x.y])
        idx_of_near_towrs = self.towers_c_qt.query_ball_point(tx, self.search_radius) # type: ignore
        for connected_twr in conns_w_tower_x:
            for idx, near_twr in self.towers_c.filter(items=idx_of_near_towrs,
                                                      axis=0).iterrows():
                near_twr = Point2D(near_twr['m'], near_twr['p'], id=idx)
                if self._comparable(tower_x, connected_twr, near_twr):
                    if self._t2_btwin_tx_t1(tower_x, connected_twr, near_twr):
                        self._connections[tower_x.id][connected_twr.id] = 0
                        self._connections[connected_twr.id][tower_x.id] = 0
                        break


    def _t2_btwin_tx_t1(self, tx, t1, t2) -> bool:
        tx  = np.array([tx.x, tx.y])
        t1  = np.array([t1.x, t1.y])
        t2  = np.array([t2.x, t2.y])
        vx2 = t2 - tx
        vx1 = t1 - tx
        w   = np.dot(vx2, vx1) / np.dot(vx1, vx1)
        pw  = tx + w * vx1
        vw2 = t2 - pw
        sqdist_transv = np.dot(vw2, vw2)
        if sqdist_transv < self._lat_buff_2_del_con*self._lat_buff_2_del_con:
            if w >= 0 and w <= 1:
                return True
        return False


    def _ext_fix_conns(self, ext_conns: dict):        
        for con in ext_conns:
            try:
                if not (str(con[0]).isdigit() and str(con[1]).isdigit()):
                    continue
                if con[2]:
                    twr1 = Point2D(x=self.towers_c.loc[int(con[0])]['m'], 
                                y=self.towers_c.loc[int(con[0])]['p'], 
                                id=int(con[0]))
                    twr2 = Point2D(x=self.towers_c.loc[int(con[1])]['m'], 
                                y=self.towers_c.loc[int(con[1])]['p'], 
                                id=int(con[1]))
                    self.connections[int(con[0])][int(con[1])] = twr2
                    self.connections[int(con[1])][int(con[0])] = twr1
                else:
                    self.connections[int(con[0])][int(con[1])] = 0
                    self.connections[int(con[1])][int(con[0])] = 0
            except KeyError:
                logging.exception('Twr to fix out of bounds.')
                print(f'Twr to fix out of bounds: {con[0]} - {con[1]}')
                logging.error(f'Twr1: {con[0]}; Twr2: {con[1]}')
            except:
                logging.exception('')
                logging.error(f'Twr1: {con[0]}; Twr2: {con[1]}')
            finally:
                continue


    def _remove_triagulations(self):        
        for twr_id, row in enumerate(self.connections):
            m = self.towers_c.iloc[twr_id]['m']
            p = self.towers_c.iloc[twr_id]['p']
            twr = Point2D(m, p, id = twr_id)
            twr_cons = self._get_conns(twr)
            try:
                for con1 in twr_cons:
                    for con2 in self._get_conns(con1):
                        if (con2.id != twr.id and 
                            isinstance(self.connections[twr.id][con2.id], Point2D)):
                            dists   = self._get_distances(twr, con1, con2)                        
                            idx_max = dists.index(max(dists))
                            cons_to_del = [twr.id, con1.id] if idx_max == 0 else \
                                        [twr.id, con2.id] if idx_max == 1 else \
                                        [con1.id, con2.id]
                            self.connections[cons_to_del[0]][cons_to_del[1]] = 0
                            self.connections[cons_to_del[1]][cons_to_del[0]] = 0
            except Exception as e:
                logging.exception('')
                logging.error(f'Twr1: {twr.id}; Twr2: {con1.id}; Twr3: {con2.id}') 


    def _get_conns(self, twr: Point2D) -> list[Point2D]:
        return [tower for tower in self.connections[twr.id] if isinstance(tower, Point2D)]


    def _get_distances(self, twr1: Point2D, twr2: Point2D, twr3: Point2D) -> list:
        t1 = np.array([twr1.x, twr1.y])
        t2 = np.array([twr2.x, twr2.y])
        t3 = np.array([twr3.x, twr3.y])
        return [np.linalg.norm(t1 - t2), np.linalg.norm(t1 - t3), np.linalg.norm(t2 - t3)]


    def _zones_b2win_twrs(self, twr1: Point2D, twr2: Point2D, max) -> list[np.ndarray]:
        p1 = twr1 if twr1.x < twr2.x else twr2
        p2 = twr1 if twr1.x >= twr2.x else twr2
        max +=2
        return [np.array([p1.x + (p2.x - p1.x)*part/max, p1.y - (p1.y - p2.y)*part/max])
            for part in range(2, max)]

    
    def _count_pts_in_zones(self, zones):
        zones_wo_pts = 0
        zones_pt_count = []
        for zone in zones:
            count = len(zone)
            zones_pt_count.append(count)
            if count == 0:
                zones_wo_pts += 1
        return zones_wo_pts, zones_pt_count


    def _comparable(
        self, tower_x: Point2D, connected_twr: Point2D, near_twr: Point2D) -> bool:
        return (connected_twr != near_twr 
            and tower_x != near_twr 
            and self._has_connections(near_twr))


    def _has_connections(self, twr: Point2D) -> bool:
        return any(value for value in self.connections[twr.id] if isinstance(value, Point2D))


    def _sqdist_to_centerline(
            self, zones: list[np.ndarray], twr1: Point2D, twr2: Point2D) -> list[float]:
        t1  = np.array([twr1.x, twr1.y])
        t2  = np.array([twr2.x, twr2.y])
        return [self._calc_sqdist(zone, t1, t2)
            for zone in zones]    
        

    def _calc_sqdist(self, zone: np.ndarray, t1: np.ndarray, t2: np.ndarray) -> float:
        if len(zone) == 0:
            return 9999
        scale = self.cables.scale
        p = np.array([np.mean(zone['X'])*scale, np.mean(zone['Y'])*scale])
        v1p = p - t1
        v12 = t2 - t1
        w   = np.dot(v1p, v12) / np.dot(v12, v12)
        pw  = t1 + w * v12
        vpw = p - pw
        return np.dot(vpw, vpw)


    def _angle_b2win(self, twr1: Point2D, twr2: Point2D, zones: list[np.ndarray]) -> float:
        t1 = np.array([twr1.x, twr1.y])
        t2 = np.array([twr2.x, twr2.y])
        twrs_slope = _slope(t1, t2)
        zones_slope, intercept, r, p, std_err = self._linregress(zones)
        return _to_deg(math.atan2(twrs_slope, 1) - math.atan2(zones_slope, 1)) #type:ignore


    def _linregress(self, zones: list[np.ndarray]) -> tuple:
        xx, yy = self._zones_coords(zones)
        return stats.linregress(xx, yy)

    
    def _zones_coords(self, zones: list[np.ndarray]) -> tuple[list[Any], list[Any]]:
        scale = self.cables.scale
        coords = [[np.mean(zone['X'])*scale, np.mean(zone['Y'])*scale]
              for zone in zones
              if  zone.size != 0]
        return [item[0] for item in coords], [item[1] for item in coords]


def _slope(t1: np.ndarray, t2: np.ndarray) -> float:
    return (t2[1] - t1[1]) / (t2[0] - t1[0])


def _to_deg(angle: float) -> float:
    return angle * 180 / math.pi #type:ignore
    

if __name__ == "__main__":
    a = [[1,2,3],[1,1.5,2]]
    slope, intercept, r, p, std_err =stats.linregress(a[0], a[1])
    print(slope)