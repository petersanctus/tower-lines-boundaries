import laspy
import numpy as np
from tqdm import tqdm
import concurrent.futures
import logging
import os

RETURN_NUMBER_MASK_0 = 0b00000111
NUMBER_OF_RETURNS_MASK_0 = 0b00111000
CLASSIFICATION_MASK_0 = 0b00011111

def fix_las_rn(las_file):
    all = las_file.points.array
    cpu_avalable = os.cpu_count()
    splitted = np.array_split(all, cpu_avalable)
    with concurrent.futures.ProcessPoolExecutor() as executer:
        results_rem = executer.map(_check_rn, splitted)
    count = 0  
    for results, pts in zip(results_rem, splitted):
        _change_bitfields(results, pts)
        count += len(results)
    logging.info(f'Corrected {count} return number record(s).')
    las_new = laspy.create(point_format=las_file.header.point_format, 
                           file_version=las_file.header.version)
    las_new.header = las_file.header
    las_new.points.array = all            
    las_new.update_header()
    return las_new


def _check_rn(pts):
    pts_to_correct = {}
    it = 0
    for line, line2 in tqdm(zip(pts[0:-1:1], pts[1:-1:1]), desc='Check rn:', total=len(pts)):
        l1_rn = line['bit_fields'] & RETURN_NUMBER_MASK_0
        l2_rn = line2['bit_fields'] & RETURN_NUMBER_MASK_0
        l2_nr = (line2['bit_fields'] & NUMBER_OF_RETURNS_MASK_0) >> 3
        l1_t = line['gps_time']
        l2_t = line2['gps_time']
        if (l2_t != l1_t and l2_rn > 1) or \
           (l2_t == l1_t and l2_rn -1 > l1_rn):
            pts_to_correct[it+1] = l2_rn - 1 + ((l2_nr - 1) << 3)
        it += 1
    return pts_to_correct
  

def _change_bitfields(rn_to_change, pts):
    for idx, value in rn_to_change.items():
        pts[idx]['bit_fields'] = value    