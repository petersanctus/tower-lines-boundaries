from io import TextIOWrapper
from numpy.lib.function_base import copy
from scipy.spatial import cKDTree
from laspy import lasdata
import numpy as np
from matplotlib import pyplot
from pandas import DataFrame
from plboundaries.baseclasses.point import *
from plboundaries.baseclasses.circle import Circle
from sklearn.cluster import DBSCAN
from tqdm import tqdm
import copy
import logging
import time

class Towers_Ident:

    def __init__(self):
        self._id_pos          = {}
        self._towers_centroid = DataFrame(columns = ['m', 'p', 'h'])
        twr_c_dtypes          = {'m': float, 'p': float, 'h': float}
        self._towers_centroid = self._towers_centroid.astype(twr_c_dtypes)
        self._towers_count    = 0

    @property
    def las_file(self) -> lasdata:
        return self._las_file

    @property
    def gr_idx(self) -> dict:
        return self._gr_idx 

    @property
    def scale(self) -> int:
        return self._scale

    @property
    def count(self) -> int:
        return self._towers_count

    @property
    def towers_centroid(self) -> DataFrame:
        """
        Return a Pandas DataFrame with the centroid of each tower [Id X Y Z]
        """
        return self._towers_centroid

    @towers_centroid.setter
    def towers_centroid(self, twrs_centroid: DataFrame):
        self._towers_centroid = twrs_centroid


    @property
    def qt_centroids(self) -> cKDTree:
        return self._2dtree_of_centroids()

    def renew_twr_points(self, las_file: lasdata):
        twr_c = self._towers_centroid
        self._scale    = las_file.header.scales[0] # type: ignore
        self._las_file = las_file
        if len(self.las_file.points.array) == 0: # type: ignore
            self._towers_count = 0
            logging.warning(r'No tower points found in las file')
            return
        first_pt = Circle(LasPoint_3(las_file.points.array[0], self.scale), 6) # type: ignore
        near_c_gr = self._near_gr_in_centroids(first_pt)               
        if near_c_gr:
            group = near_c_gr[0]
        else:
            group = twr_c.last_valid_index() + 1 if not twr_c.empty else 0 # type: ignore
        self._gr_idx = {group: [0]}


    def group_by_tower(self, buffer_radius: int):
        if len(self.las_file.points.array) == 0: # type: ignore
            logging.warning(r'Group towers is not possible due to no tower points available')
            return
        pts = np.column_stack([self.las_file.points.array[1:]['X']*self.scale, # type: ignore
                           self.las_file.points.array[1:]['Y']*self.scale]) # type: ignore
        start = time.perf_counter()
        clustering = DBSCAN(eps=buffer_radius, min_samples=5, leaf_size=100).fit(pts)        
        print(f'Clustering time: {round(time.perf_counter()-start,2)}')
        for idx, group in enumerate(clustering.labels_.T):
            if group == -1:
                continue
            if not group in self._gr_idx:
                self._gr_idx[group] = [idx+1]
            else:
                self._gr_idx[group].append(idx+1)


    def calculate_centroid(self):
        centroids = []
        if len(self.las_file.points.array) == 0: # type: ignore
            logging.warning('Calculate centroids is not '
                            'possible due to no tower points available')
            return
        total_c = len(self._gr_idx)
        for id in tqdm(self._gr_idx.keys(), desc="Calculate centroids: ", total=total_c):
            twr = np.flip(np.sort(
                self._las_file.points.array[self._gr_idx[id]], order=['Z'])) # type: ignore
            filter = self._get_sample([2, 7], twr, id)
            if not True in filter:
                continue            
            twr_filtered = twr[filter]
            twr_filtered = twr
            x_mean = np.median(twr_filtered['X']) * self.scale
            y_mean = np.median(twr_filtered['Y']) * self.scale
            z_mean = np.median(twr_filtered['Z']) * self.scale
            centroids.append([id, x_mean, y_mean, z_mean])
        self._update_tower_centroid(centroids)
        logging.info(f'Defined {len(self.towers_centroid)} tower centroid(s).')
        self._towers_count = len(self.gr_idx)


    def print_towers_groups(self, stream_write: TextIOWrapper):
        try:
            if len(self._las_file.points.array) == 0: # type: ignore
                logging.warning(r'No tower to print')
                return
            s = self.scale
            for id in self._gr_idx.keys():
                twr = self._las_file.points.array[self._gr_idx[id]] # type: ignore
                with np.nditer(twr, op_flags=['readonly']) as it:
                    for line in it:
                        stream_write.write( 
                            f'{line["X"]*s} {line["Y"]*s} {line["Z"]*s} {id}\n') 
        except Exception:
            logging.exception((''))


    def print_towers_centroid(self, stream_write: TextIOWrapper):
        try:
            if len(self._las_file.points.array) == 0: # type: ignore
                logging.warning(r'No tower to print')
                return
            self.towers_centroid.to_csv(stream_write,
                                        sep=' ',
                                        header=False,
                                        index=True,
                                        float_format='%.2f')
        except Exception as e:
            logging.error(f'{e}')


    def tower_plot(self):
        ax1 = self.towers_centroid.plot.scatter(x='m',
                                                y='p',
                                                colormap='viridis',
                                                s=2)
        pyplot.show()


    def _new_dataframe(self, data: list) -> DataFrame:
        twr_c_dtypes = {'id': int, 'm': float, 'p': float, 'h': float}
        df           = DataFrame(data, columns = ['id', 'm', 'p', 'h'])
        df           = df.astype(twr_c_dtypes)
        df.set_index('id', inplace = True)
        return df


    def _update_v_search_range(self, v_search_range, twr_id):
        new_range = list(map(lambda x:x+3, v_search_range))
        logging.debug(f"Tower {twr_id}: search zone changed to " \
            + f"[{new_range[0]}, {new_range[1]}]")
        return new_range


    def _get_sample(self, v_search_range, twr, twr_id):
        twr_top = twr[0]
        scale = self.scale
        while(v_search_range[1] < 30):
            upper = twr_top['Z']*scale - v_search_range[0]
            bottom = twr_top['Z']*scale - v_search_range[1]
            filter = (twr[:]['Z']*scale < upper)[:] & \
                     (twr[:]['Z']*scale > bottom)[:]      
            if len(twr[filter]) > 10:
                return filter
            v_search_range = self._update_v_search_range(v_search_range, twr)
        logging.debug(f'Tower {twr_id} will use data from 1st meters.')
        return (twr[:]['Z']*scale > (twr_top['Z']*scale - 2))[:]


    def _update_tower_centroid(self, centroids: list):
        if self._towers_centroid.empty:
            self._towers_centroid = self._new_dataframe(centroids)
        else:
            for centroid in centroids:
                c_id = centroid[0]
                if c_id not in self._towers_centroid.index:
                    last_idx = self._towers_centroid.index[-1]
                    self._towers_centroid.at[last_idx + 1] = centroid[1:]
                else:
                    if self._towers_centroid.iloc[c_id]['h'] < centroid[3]: # type: ignore
                        self._towers_centroid.iloc[c_id] = centroid[1:]
        self._verify_near_centroids()


    def _find_near_group(self, pt_to_verify: Circle) -> int:
        near_c_gr = self._near_gr_in_centroids(pt_to_verify)               
        if near_c_gr:
            return near_c_gr[0]
        near_gr = self._near_gr_in_gr_idx(pt_to_verify)
        if near_gr:
            near_gr.sort()
            return near_gr[0]
        twr_c = self._towers_centroid 
        if not twr_c.empty:
            keys_not_centroid = self._last_key(self._gr_idx) > twr_c.last_valid_index() # type: ignore
        else:
            keys_not_centroid = True
        new_gr = (self._last_key(self._gr_idx) + 1 
               if self._gr_idx and keys_not_centroid 
             else twr_c.last_valid_index() + 1 if not twr_c.empty # type: ignore
             else 0)
        return new_gr

    
    def _last_key(self, dic: dict) -> int:
        return list(dic.keys())[-1]


    def _verify_near_centroids(self):        
        qt_centroids = self.qt_centroids
        new_centroids = copy.deepcopy(self._towers_centroid)
        idx_to_drop = []
        for idx, centroid in self._towers_centroid.iterrows():
            np_centroid = np.array([centroid[0], centroid[1]])
            near_centroids = qt_centroids.query_ball_point(np_centroid, 3.5) # type: ignore         
            if len(near_centroids) > 1:
                near_centroids.sort()
                mean = self._towers_centroid[['m', 'p']].iloc[near_centroids].mean(axis=0) # type: ignore
                new_centroids['m'].iloc[near_centroids[0]] = mean['m']
                new_centroids['p'].iloc[near_centroids[0]] = mean['p']
                idx_to_drop += near_centroids[1:]
        idx_to_drop = list(set(idx_to_drop))
        idx_to_drop.sort(reverse=True)
        new_centroids.drop(idx_to_drop, axis=0, inplace=True, errors='ignore')
        self._towers_centroid = new_centroids
        self._towers_centroid.reset_index(drop=True, inplace=True)


    def _2dtree_of_centroids(self) -> cKDTree:
        m = [x for x in self.towers_centroid['m']]
        p = [y for y in self.towers_centroid['p']]
        data_xy = np.array((m,p)).transpose()
        return cKDTree(data_xy) # type: ignore   
   

    def _near_gr_in_centroids(self, pt_to_verify: Circle):
        return self.qt_centroids.query_ball_point(pt_to_verify.numpy_2D, 6) # type: ignore   


    def _near_gr_in_gr_idx(self, pt_to_verify):
        twr_pts = self.las_file.points.array # type: ignore   
        grs = list(self._gr_idx.keys())
        it_intervals = self._get_it_intervals(grs)
        for start, end in it_intervals:
            for gr in grs[start:end:1]:
                idx = self._gr_idx[gr][0]
                twr = LasPoint_3(twr_pts[idx], self.scale)
                if pt_to_verify.contains_point(
                    LasPoint_3(twr_pts[idx], self.scale)):
                    return [gr]
        return []
        
    
    def _get_it_intervals(self, array):
        it_num = []
        for it in range(4, len(array) + 1, 4):
            i = -it
            f = len(array) if it == 4 else i + 4
            it_num.append([i, f])
        it_num.append([0, len(array)%4])
        return it_num
 