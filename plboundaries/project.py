import logging as log
from laspy.lasdata import LasData
from plboundaries.baseclasses.point import *
from plboundaries.baseclasses.cables import Cables
from plboundaries.jobs.identify_towers import Towers_Ident
from plboundaries.jobs.conect_towers import Conect_towers
from plboundaries.jobs.crop_to_span import Crop_Las
from plboundaries.tools import project_tools
from plboundaries.tools import delivers
from plboundaries.tools import imports
from pathlib import Path
import os


class Project():

    def __init__(self, work_folder: str, prj_name: str, year: int, data_folder: Path):        
        self._twr_ident   = Towers_Ident()
        self._spans_found = []
        self._work_folder = work_folder
        self._ext_twr_names = ""
        self._data_folder = data_folder
        self._prj_name = prj_name
        self._year = year
        self._twrs_wout_cons = []
        self._twrs_renamed = []
        self._older_twrs = f'{self.work_folder}\\new_tower_names.txt'
        if not os.path.exists(self.older_twr_names):
            with open(self.older_twr_names, 'w') as tn_fout:
                tn_fout.write('')
    
    # file adress with new names which are created in the process
    @property
    def older_twr_names(self) -> str:
        return self._older_twrs
    
    @property
    def twrs_wout_cons(self) -> list[Point2D]:
        return self._twrs_wout_cons
    
    @property
    def twrs_renamed(self) -> list:
        return self._twrs_renamed
    
    @property
    def work_folder(self):
        return self._work_folder
    
    @work_folder.setter
    def work_folder(self, value: str):
        self._work_folder = value
    
    @property
    def data_folder(self) -> Path:
        return self._data_folder        
        
    
    @data_folder.setter
    def data_folder(self, value: Path):
        self._data_folder = value
    
    # external file
    @property
    def ext_twr_names(self) -> str:
        return self._ext_twr_names
    
    @ext_twr_names.setter
    def ext_twr_names(self, file_location: str):
        self._ext_twr_names = file_location
        
    @property
    def prj_name(self):
        return self._prj_name
    
    @prj_name.setter
    def prj_name(self, prj_name: str):
        self._prj_name = prj_name
        
    @property
    def year(self):
        return self._year
    
    @year.setter
    def year(self, year):
        self._year = year
        
    @property
    def output_folder(self):
        return rf'{self.work_folder}\{self.prj_name}'
        
    @property
    def spans_found(self):
        return self._spans_found
        
    @property
    def twr_ident(self) -> Towers_Ident:
        return self._twr_ident
        
    @property
    def twrs_prefix(self):
        return self._twrs_prefix
        
    @property
    def twrs_sufix(self):
        return self._twrs_sufix

    @property
    def twr_las_data(self) -> LasData:
        return self._towers

    @property
    def cables_las_data(self) -> LasData:
        return self._cables

    @property
    def backup_dir(self) -> Path:
        return Path(self.work_folder) / 'tmp_las_data' / self.prj_name

    def import_twrs_and_cables(self) -> None:
        try:
            data_folder = ( self.backup_dir 
                         if imports.local_backup(self._data_folder, self.backup_dir) 
                       else self._data_folder)        
            files_to_import = self.get_las_files(data_folder)  
            self._towers, self._cables = \
                imports.twrs_and_cables(
                    files_to_import, 
                    twrs_sample=5, 
                    cables_sample=1)
        except IOError:
            log.exception('')
            print('Error in mport twrs & cables. See log for more information.')
            quit()
        except Exception:
            log.exception('')
            print('Error in mport twrs & cables. See log for more information.')
            quit()

    
    def identify_towers(self, search_buffer=6):
        self._twr_ident.renew_twr_points(self._towers)
        self._twr_ident.group_by_tower(search_buffer)
        self._twr_ident.calculate_centroid()


    def find_connections(self, proc_options):
        fix_conns = imports.import_external_connections(self.work_folder, 
                                                        proc_options['fix_conns_file'], 
                                                        self.prj_name)
        connections = Conect_towers(Cables(self._cables), 
                                    self._twr_ident.towers_centroid, 
                                    proc_options)        
        connections.find_connections(fix_conns)
        self._twrs_wout_cons = connections.check_towers_with_no_connections()
        self._spans_found    = connections.spans_found(proc_options)
    

    def rename_twrs(self, prefix='', sufix=''):
        self._twrs_prefix = prefix
        self._twrs_sufix = sufix
        if prefix != '' or sufix != '':
            self._spans_found = project_tools.add_sufix_prefix_to_twrs_id(self)
        self._spans_found = project_tools.rename_twrs(self)

    
    def crop_las_to_span_dirs(self, sampling = 5, skip=0):
        delivers.check_folder_exists(self.output_folder)
        seeder = Crop_Las(self, sampling)
        files_to_crop = self.get_las_files(self.data_folder, skip)
        delivers.crop_las_data(seeder, files_to_crop)


    def output_to_dxf(self):
        output_folder = f'{self.output_folder}\\review'
        delivers.check_folder_exists(output_folder)
        delivers.output_to_dxf(self, output_folder)
    

    def correct_rn(self, files_folder: Path, output_folder: str):
        files_to_crop = self.get_las_files(files_folder)
        delivers.check_folder_exists(output_folder)
        project_tools.correct_rn(files_to_crop, output_folder)


    def output_twrs_to_txt(self):
        output_folder = f'{self.output_folder}\\log'
        delivers.check_folder_exists(output_folder)
        delivers.output_twrs_to_txt(self, output_folder)
        delivers.output_spans(self, output_folder)
    

    def merge_twr_names_files(self):
        delivers.check_folder_exists(self.output_folder)
        project_tools.merge_twr_names_files(self)
        

    def output_twrs_and_cables_las(self):
        output_folder = f'{self.output_folder}\\review'
        delivers.check_folder_exists(output_folder)
        delivers.output_tc_las(self, output_folder)


    def get_las_files(self, path: Path, skip=0) -> list:
        output_folder = f'{self.output_folder}\\log'
        delivers.check_folder_exists(output_folder)
        files = list(item 
                for  item in path.glob('**/*') 
                if   item.is_file() and 'las' in item.suffix)[skip:]
        with open(f'{output_folder}\\list_of_las_files.txt', 'w') as list_out:
            delivers.print_list(list_out, files)
        if files == []:
            raise IOError(f'Data folder is empty: {path.anchor}')
        return files
