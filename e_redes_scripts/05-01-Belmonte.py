from plboundaries.project import Project
from plboundaries.jobs.search_copy_spandirs import Copy_spans
import time
import logging
import os

def crop_macro(input_line: Project, proc_options):
    start = time.perf_counter()
    logging.info(f'++++ Crop Macro initiated on project: {input_line.prj_name}:')
    input_line.ext_twr_names = f'{proc_options["work_folder"]}\\{proc_options["twr_names_file"]}'
    if not input_line.import_twrs_and_cables():
        return 0
    input_line.identify_towers(search_buffer = proc_options["search_buffer_cluster_twrs"])
    input_line.find_connections(proc_options)
    input_line.rename_twrs(prefix=proc_options['prefix'])
    input_line.output_twrs_to_txt()
    input_line.output_to_dxf()
    input_line.output_twrs_and_cables_las()
    if proc_options['crop_files']:
        input_line.crop_las_to_span_dirs(sampling=proc_options['sample_step'], 
                                         skip=proc_options['skip files to crop'])
    input_line.merge_twr_names_files()
    finish = time.perf_counter()
    logging.info(f'----------------------Crop Macro finished in {round(finish-start,2)} second(s)')
    return input_line.twr_ident.count


def correct_rn_macro(line_test: Project):
    start = time.perf_counter()
    logging.info(f'++++ Correct Return Numbers Macro initiated on project: {line_test.prj_name}:')
    line_test.correct_rn(line_test.data_folder, 
                         f'{line_test.data_folder}\\RN_Fixed')
    finish = time.perf_counter()
    logging.info(f'----------------------Correct Return Numbers Macro finished in {round(finish-start,2)} second(s)')


def initialize_process(proc_options):
    logging.basicConfig(filename=f'{proc_options["work_folder"]}\\tlboundaries.log',
                                    level=logging.INFO,
                                    format='%(asctime)s %(levelname)s: %(message)s -- %(module)s>>%(funcName)s')
    with open(f'{proc_options["work_folder"]}\\new_tower_names.txt', 'w') as nt_file,\
         open(f'{proc_options["work_folder"]}\\merged_twr_file.txt', 'w') as mt_file:
            nt_file.write('')
            mt_file.write('')

if __name__ == "__main__":
    
    if True:
        working_folder = r"E:\E-Redes_WF"                           
        data_folder_2020 = f'D:\\dados_originais\\2020'
        data_folder_2019 = f'E:\\dados_originais\\2019'
        data_folder_2018 = f'F:\\dados_originais\\2018'
        data_folder_2017 = f'F:\\dados_originais\\2017'
        data_folder_2016 = f'G:\\dados_originais\\2016'
        data_folder_2016 = f'E:\E-redes_Data'

        folders_to_process = {
                                'C1606255':('1606267',2016),
                                'C1606258':('1606268',2016),
                                'C1606271':('1606272',2016),
                                'C1705418':('1705305',2017),
                                'C1802821':('1802828',2018),
                                'C1802823':('1802830',2018),
                                'C2004108':('2004420',2020),
                                'C2004111':('2004421',2020),
                                'C2004117':('2004423',2020),
                                'C2004132':('2004425',2020),
                                'C1902173':('1902696',2019),
                                'C1903315':('1903680',2019),
                                'C1805838':('1806055',2018),
                             }

        proc_options = {'work_folder'                : working_folder,
                        'data_folder'                : data_folder_2016,
                        'skip files to crop'         : 0,
                        'twr_count'                  : 1,
                        'sample_step'                : 5,
                        'twr_names_file'             : '',
                        'fix_conns_file'             : '',
                        'prefix'                     : '05_01_P.',
                        'search_buffer_cluster_twrs' : 2,
                        'transv_buff'                : 25,
                        'long_buff'                  : 10,
                        'search_connec_twr_radius'   : 500,
                        'lat_buff_to_check_twr_b2win': 5,
                        'search_cable_radius'        : 2,
                        'crop_files'                 : True  
                        }

        
        initialize_process(proc_options)
        for it, line_folder in enumerate(os.listdir(proc_options['data_folder'])):
            if not line_folder in folders_to_process.keys():
                continue
            print(f'\nProcessing {folders_to_process[line_folder][0]}:\n')
            process              = Project(proc_options['work_folder'])
            process.prj_name     = f'{folders_to_process[line_folder][0]}'
            process.year         = folders_to_process[line_folder][1]
            process.data_folder  = f'{proc_options["data_folder"]}\\{line_folder}'
            proc_options['twr_count'] += crop_macro(process, proc_options) + 1

    if False:
        f2020              = r'D:\dados_separados\05 - CB\05 - Idanha'
        f2019              = r'E:\dados_separados\05 - CB\03 - Covilha'
        f2018              = r'F:\dados_separados\05 - CB\05 - Idanha'
        destination_folder = r'C:\Users\Python\Documents\E-Redes\Delivers'
        children_folder    = []
        copy_job           = Copy_spans(f2018, children_folder, destination_folder)
        if True:
            with open(r'C:\Users\Python\Documents\E-Redes\Delivers\Idanha_2018_unmached_spans.txt', 'r') as f_in:
                list_spans_to_copy = f_in.read().splitlines()
        copy_job.copy_spans('')
    
