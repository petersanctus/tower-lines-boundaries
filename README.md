# Power Lines Boundaries

Package to crop LAS data into span boundaries and exports to txt files.

The spans are created based on the search for transmission towers and the lines that connects them in the LiDAR data.

## Installation

Run the following to install:

```
pip install git+http://grou/dds/tower-lines-boundaries.git
```

### Usage:

```python
from plboundaries.project import Project
import time
import logging
import os

def crop_macro(line_test: Project, twr_names_file):
    start = time.perf_counter()
    logging.info(f'++++ Crop Macro initiated on project: {line_test.prj_name}:')
    line_test.ext_twr_names = twr_names_file
    line_test.import_twrs_and_cables()
    line_test.identify_towers(search_buffer=6)
    line_test.find_connections(transv_buff=15, long_buff=10)
    line_test.rename_twrs()    
    line_test.output_twrs_to_txt()
    line_test.output_to_dxf()
    line_test.crop_las_to_span_dirs(sampling=5)

    finish = time.perf_counter()
    logging.info(f'----------------------Crop Macro finished in {round(finish-start,2)} second(s)')


def correct_rn_macro(line_test: Project):
    start = time.perf_counter()
    logging.info(f'++++ Correct Return Numbers Macro initiated on project: {line_test.prj_name}:')
    line_test.correct_rn(line_test.data_folder, f'{line_test.data_folder}\\RN_Fixed')
    finish = time.perf_counter()
    logging.info(f'----------------------Correct Return Numbers Macro finished in {round(finish-start,2)} second(s)')


if __name__ == "__main__":
    working_folder = r"C:\Users\Python\Documents\Prj_Linha_Teste"
    logging.basicConfig(filename=f'{working_folder}\\tlboundaries.log',
                        level=logging.INFO,
                        format='%(asctime)s %(levelname)s: %(message)s -- %(module)s>>%(funcName)s')                        
    years_folder = f'{working_folder}\\Dados Originais'
    for year in os.listdir(years_folder):        
        line_test = Project()
        line_test.work_folder = working_folder        
        line_test.prj_name = 'Linha Amostra'  
        line_test.year = int(year)
        # correct_rn_macro(line_test)
        if year == '2020':
            line_test.data_folder = f'{years_folder}\\{year}\\RN_Fixed'
        line_test.data_folder = f'{years_folder}\\{year}' 
        crop_macro(line_test, twr_names_file=f'{line_test.work_folder}\\twrs_names.txt')
```
