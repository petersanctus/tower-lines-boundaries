""" import sys
sys.path.append(r'F:\\Trabalho\Albatroz\Data Analysis\\Tower Lines') """

from plboundaries.project import Project
import time
import logging
import os


def crop_macro(input_line: Project, twr_names_file='', sample_step=5, twr_counter=0):
    start = time.perf_counter()
    logging.info(f'++++ Crop Macro initiated on project: {input_line.prj_name}:')
    input_line.ext_twr_names = twr_names_file
    input_line.import_twrs_and_cables()
    input_line.identify_towers(search_buffer=7)
    input_line.find_connections(transv_buff=25, long_buff=10, search_radius=900, twr_increment=twr_counter)
    input_line.rename_twrs(prefix='05_06_P.')
    input_line.output_twrs_to_txt()
    input_line.output_to_dxf()
    #input_line.crop_las_to_span_dirs(sampling=sample_step)
    finish = time.perf_counter()
    logging.info(f'----------------------Crop Macro finished in {round(finish-start,2)} second(s)')
    return input_line.twr_ident.count


def correct_rn_macro(line_test: Project):
    start = time.perf_counter()
    logging.info(f'++++ Correct Return Numbers Macro initiated on project: {line_test.prj_name}:')

    line_test.correct_rn(line_test.data_folder, 
                         f'{line_test.data_folder}\\RN_Fixed')

    finish = time.perf_counter()
    logging.info(f'----------------------Correct Return Numbers Macro finished in {round(finish-start,2)} second(s)')


if __name__ == "__main__":    
    # working_folder = r"D:\Trabalho\Albatroz\Projetos\E-Redes\Out_Las\test_mult_las_files"
    twr_count = 0
    working_folder = r"D:\Tmp\E-redes"
    logging.basicConfig(filename=f'{working_folder}\\tlboundaries.log',
                        level=logging.INFO,
                        format='%(asctime)s %(levelname)s: %(message)s -- %(module)s>>%(funcName)s')    
    data_folder = f'{working_folder}\\dados_originais\\2020'
    data_folder = f'{working_folder}\\dados_originais\\Testes'
    folders_to_process = {'C2004124':'2004110','C2004125':'2004111'}
    for line_folder in os.listdir(data_folder): 
        if not line_folder in folders_to_process.keys():
            continue
        vila_de_rei = Project(working_folder ) 
        vila_de_rei.prj_name = f'{folders_to_process[line_folder]}'  
        vila_de_rei.year = 2020
        # correct_rn_macro(line_test)        
        vila_de_rei.data_folder = f'{data_folder}\\{line_folder}\\TSCAN\\TILES'
        vila_de_rei.data_folder = f'{data_folder}\\{line_folder}\\'
        twr_count += crop_macro(vila_de_rei, twr_names_file=f'{working_folder}\\towers_names.txt', sample_step=5, twr_counter=twr_count + 1)
        

    #correct_rn_macro(line_test)

    
    