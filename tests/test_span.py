import os
import sys
""" BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
print(BASE_DIR)
sys.path.insert(0, BASE_DIR)
 """
from plboundaries.baseclasses.span import Span
from plboundaries.baseclasses.point import Point2D
import unittest
from plboundaries.baseclasses.rectangle import Rectangle

class TestSpan(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_create_span(self):
        twr_1 = Point2D(1,1)
        twr_2 = Point2D(10,2)
        span_test = Span(twr_1, twr_2)
        self.assertIsInstance(span_test, Span)
        self.assertEqual(span_test.tower_1, twr_1)
        self.assertEqual(span_test.tower_2, twr_2)
        self.assertEqual(span_test.buff_transv, 0)
        self.assertEqual(span_test.long_buff, 0)


    def test_contains_point(self):
        twr_1 = Point2D(1,1)
        twr_2 = Point2D(10,1)
        span_test = Span(twr_1, twr_2, 10, 1)
        point_test = Point2D(2,2)
        self.assertTrue(span_test.contains_point(point_test))
        point_test = Point2D(10.1,3)
        self.assertTrue(span_test.contains_point(point_test))
        point_test = Point2D(.1,0)
        self.assertTrue(span_test.contains_point(point_test))
        point_test = Point2D(0,11)
        self.assertTrue(span_test.contains_point(point_test))
        point_test = Point2D(11,11)
        self.assertTrue(span_test.contains_point(point_test))
        point_test = Point2D(11,-9)
        self.assertTrue(span_test.contains_point(point_test))
        point_test = Point2D(0,-9)
        self.assertTrue(span_test.contains_point(point_test))
        twr_1 = Point2D(562475.40,4479603.65)
        twr_2 = Point2D(562632.41,4479560.99)
        twr_3 = Point2D(562382.90,4479629.11)
        span_test = Span(twr_1, twr_2, 10, 10)
        boundary = ([562468.3718, 4479615.9221], [562644.6821, 4479568.0182],
                    [562639.4382, 4479548.7179], [562463.1279, 4479596.6218],
                    [562468.3718, 4479615.9221] )
        self.assertEqual(span_test.boundary, boundary)
        span_test = Span(twr_1, twr_2, 10, 1)
        boundary = ([562477.057, 4479613.5623], [562635.997, 4479570.3779],
                    [562630.753, 4479551.0777], [562471.813, 4479594.2621],
                    [562477.057, 4479613.5623] )
        self.assertEqual(span_test.boundary, boundary)
        span_test = Span(twr_1, twr_3, 10, 1)
        boundary = ([562473.7104, 4479593.7432], [562379.2821, 4479619.7339],
                    [562384.5896, 4479639.0168], [562479.0179, 4479613.0261],
                    [562473.7104, 4479593.7432] )
        self.assertEqual(span_test.boundary, boundary)

    
    def test_intersects_boundary(self):
        boundary = Rectangle(Point2D(562427.90,4479635.31),25,4)
        twr_1 = Point2D(562475.40,4479603.65)
        twr_3 = Point2D(562382.90,4479629.11)
        span_test = Span(twr_1, twr_3, 10, 1)
        self.assertTrue(span_test.intersects_boundary(boundary))
        boundary = Rectangle(Point2D(562407.52,4479641.17),25,4)
        self.assertTrue(span_test.intersects_boundary(boundary))
        twr_1 = Point2D(562202.54,4479577.99)
        twr_3 = Point2D(562212.56,4479473.19)
        span_test = Span(twr_1, twr_3, 10, 1)
        boundary = Rectangle(Point2D(562202.85,4479526.10),40,8)
        self.assertTrue(span_test.intersects_boundary(boundary))
        twr_1 = Point2D(562162.85,4479510.1)
        twr_3 = Point2D(562242.85,4479510.1)
        span_test = Span(twr_1, twr_3, 8, 1)
        self.assertTrue(span_test.intersects_boundary(boundary))
        twr_1 = Point2D(562098.56,4479488.45)
        twr_3 = Point2D(562178.56,4479488.45)        
        span_test = Span(twr_1, twr_3, 8, 1)       
        self.assertTrue(not span_test.intersects_boundary(boundary))
        twr_1 = Point2D(562632.02,4479560.67)
        twr_3 = Point2D(562755.81,4479526.09)    
        span_test = Span(twr_1, twr_3, 2, 0)      
        boundary = Rectangle(Point2D(562669.485,4479550.535),5.095,4.215)   
        self.assertTrue(span_test.intersects_boundary(boundary))


    def test_centroid(self):
        twr_1 = Point2D(1,1)
        twr_2 = Point2D(3,2)
        twr_3 = Point2D(1,3)
        twr_4 = Point2D(-3,0)
        twr_5 = Point2D(1,1)
        self.assertTrue(Span(twr_1, twr_2).centroid == Point2D(2,1.5))
        self.assertTrue(Span(twr_1, twr_3).centroid == Point2D(1,2))
        self.assertTrue(Span(twr_1, twr_4).centroid == Point2D(-1,.5))
        self.assertTrue(Span(twr_1, twr_5).centroid == Point2D(1,1))


if __name__ == '__main__':
    unittest.main()