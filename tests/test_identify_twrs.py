import unittest
import os
from plboundaries.project import Project
from plboundaries.jobs.identify_towers import Towers_Ident
from pandas import DataFrame
import laspy
from plboundaries.baseclasses.point import *
from plboundaries.baseclasses.circle import Circle
import numpy as np

class TestSpan(unittest.TestCase):

    def setUp(self):
        directory_path  = os.path.dirname(os.path.realpath(__file__))
        self.las_file   = laspy.read(f"{directory_path}\\data\\towers.las")
        self.twr_ident = Towers_Ident()
        self.twr_ident.renew_twr_points(self.las_file)
        self.scale =  self.las_file.header.scales[0]
        self.centroids = [(0, 561761.47, 4477249.32, 226.44),
                          (1, 561839.85, 4477299.16, 206.05),
                          (2, 561965.47, 4477393.11, 202.38),
                          (3, 561982.47, 4477390.73, 200.69),
                          (4, 562085.91, 4477456.47, 194.63),
                          (5, 562190.42, 4477522.99, 197.87),
                          (6, 562297.86, 4477591.30, 215.49)]
        self.towers_centroid = self.twr_ident._new_dataframe(self.centroids)


    def tearDown(self):
        pass

    def test_near_gr_in_gr_idx(self):
        self.twr_ident._gr_idx = {0:[0,1,2], 
                                  1:[2151, 2152, 2153], 
                                  2:[2928, 2929, 2930], 
                                  3:[3877, 3878, 3879], 
                                  4:[5215, 5216, 5217], 
                                  5:[5894, 5895, 5896], 
                                  6:[6332, 6333, 6334]}
        pt = Circle(LasPoint_3(self.las_file.points.array[0], self.scale), 6)
        self.assertEqual(self.twr_ident._near_gr_in_gr_idx(pt), [0])
        pt = Circle(LasPoint_3(self.las_file.points.array[2152], self.scale), 6)
        self.assertEqual(self.twr_ident._near_gr_in_gr_idx(pt), [1])
        pt = Circle(LasPoint_3(self.las_file.points.array[2929], self.scale), 6)
        self.assertEqual(self.twr_ident._near_gr_in_gr_idx(pt), [2])
        pt = Circle(LasPoint_3(self.las_file.points.array[3878], self.scale), 6)
        self.assertEqual(self.twr_ident._near_gr_in_gr_idx(pt), [3])
        pt = Circle(LasPoint_3(self.las_file.points.array[5216], self.scale), 6)
        self.assertEqual(self.twr_ident._near_gr_in_gr_idx(pt), [4])
        pt = Circle(LasPoint_3(self.las_file.points.array[5895], self.scale), 6)
        self.assertEqual(self.twr_ident._near_gr_in_gr_idx(pt), [5])
        pt = Circle(LasPoint_3(self.las_file.points.array[6333], self.scale), 6)
        self.assertEqual(self.twr_ident._near_gr_in_gr_idx(pt), [6])
        pt = Circle(Point3D(0,0,0), 6)
        self.assertEqual(self.twr_ident._near_gr_in_gr_idx(pt), [])


    def test_near_gr_in_centroids(self):
        twr_ident = self.twr_ident
        twr_ident.towers_centroid = self.towers_centroid
        pt = Point2D(561763.47, 4477247.32)
        self.assertEqual(twr_ident._near_gr_in_centroids(Circle(pt, 6))[0], 0)
        pt = Point2D(561839.85, 4477299.16)
        self.assertEqual(twr_ident._near_gr_in_centroids(Circle(pt, 6))[0], 1)
        pt = Point2D(0, 0)
        self.assertEqual(twr_ident._near_gr_in_centroids(Circle(pt, 6)), [])

    
    def test_find_near_group(self):
        twr_ident = self.twr_ident
        twr_ident.towers_centroid = self.towers_centroid
        pt = Point3D(561763.47, 4477247.32, 226.44)
        self.assertEqual(twr_ident._find_near_group(Circle(pt, 6)), 0)
        pt = Point3D(561982.47, 4477390.73, 200.69)
        self.assertEqual(twr_ident._find_near_group(Circle(pt, 6)), 3)
        pt = Circle(LasPoint_3(self.las_file.points.array[0], self.scale), 6)
        self.assertEqual(self.twr_ident._find_near_group(pt), 0)
        pt = Circle(LasPoint_3(self.las_file.points.array[2152], self.scale), 6)
        self.assertEqual(self.twr_ident._find_near_group(pt), 1)
        pt = Circle(LasPoint_3(self.las_file.points.array[2929], self.scale), 6)
        self.assertEqual(self.twr_ident._find_near_group(pt), 2)
        pt = Circle(LasPoint_3(self.las_file.points.array[3878], self.scale), 6)
        self.assertEqual(self.twr_ident._find_near_group(pt), 3)
        pt = Circle(LasPoint_3(self.las_file.points.array[5216], self.scale), 6)
        self.assertEqual(self.twr_ident._find_near_group(pt), 4)
        pt = Circle(LasPoint_3(self.las_file.points.array[5895], self.scale), 6)
        self.assertEqual(self.twr_ident._find_near_group(pt), 5)
        pt = Circle(LasPoint_3(self.las_file.points.array[6333], self.scale), 6)
        self.assertEqual(self.twr_ident._find_near_group(pt), 6)
        pt = Circle(Point3D(0,0,0), 6)
        self.assertEqual(self.twr_ident._find_near_group(pt), 7)


    def test_append_at_last(self):
        pt = Circle(LasPoint_3(self.las_file.points.array[0], self.scale), 6)
        self.assertTrue(self.twr_ident._append_at_last(pt, 0))
        self.twr_ident._gr_idx = {0:[0]}
        pt = Circle(LasPoint_3(self.las_file.points.array[0], self.scale), 6)
        self.assertTrue(self.twr_ident._append_at_last(pt, 0))
        pt = Circle(LasPoint_3(self.las_file.points.array[1], self.scale), 6)
        self.assertTrue(not self.twr_ident._append_at_last(pt, 0))
        pt = Circle(LasPoint_3(self.las_file.points.array[2152], self.scale), 6)
        self.assertTrue(self.twr_ident._append_at_last(pt, 0))

    
    def test_get_3m_sample(self):
        self.twr_ident.group_by_tower(6)  
        self.assertTrue(self._height_dif(0) < 3)
        self.assertTrue(self._height_dif(1) < 3)
        self.assertTrue(self._height_dif(2) < 3)
        self.assertTrue(self._height_dif(3) < 3)
        self.assertTrue(self._height_dif(4) < 3)
        self.assertTrue(self._height_dif(5) < 3)
        self.assertTrue(self._height_dif(6) < 3)


    def test_update_tower_centroid(self):        
        c_update = [(6, 562298, 4477592, 216),
                    (7, 562400, 4477700, 200)]
        new_c = self.twr_ident._new_dataframe(self.centroids[:-1] + c_update)
        self.twr_ident._update_tower_centroid(self.centroids)
        self.assertTrue(self.twr_ident.towers_centroid.equals(self.towers_centroid))
        self.twr_ident._update_tower_centroid(c_update)
        self.assertTrue(self.twr_ident.towers_centroid.equals(new_c))
        

    def test_calculate_centroid(self):
        self.twr_ident.group_by_tower(6)
        self.twr_ident.calculate_centroid()
        df1 = self.twr_ident.towers_centroid.round(2)
        df2 = self.twr_ident._new_dataframe(self.centroids)
        self.assertTrue(df1.equals(df2))
        

    def _height_dif(self, idx):
        sample_filter = self.twr_ident._get_3m_sample([1, 4] , idx)
        twr = self.las_file.points.array[self.twr_ident._gr_idx[idx]]
        sample = twr[sample_filter]
        max = np.amax(sample['Z']*self.scale)
        min = np.amin(sample['Z']*self.scale)
        return max - min

        

if __name__ == '__main__':
    unittest.main()